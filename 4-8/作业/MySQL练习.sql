		--连接数据库命令：
		mysql -hlocalhost -P3306 -uroot -proot;
		--创建数据库
		create database mydata; 
        create database yourdata;
        --  字符集的设置
        alter database mydata charset=utf8;
        -- 删除数据库
        drop database yourdata;
        -- 选择数据库
        use mydata;
        -- 数据表的创建
        create table mydata.user(name varchar(10), age int(3), sex varchar(2)) charset=utf8;
        create table mydata.user1(name varchar(10), age int(3), sex varchar(2)) charset=utf8;
        -- 删除数据表
        drop table user1;
        -- 修改表内字段
        alter table user change name username varchar(100);
        -- 增加表内字段
        alter table user add id int(255) first;
        -- 删除表内字段
        alter table user drop id;

        -- 新建一个 czxy 的数据库，包含两张表 user表 和 news表

		--      user： 包括以下字段
		-- 	用户-id   用户名-username  用户性别-sex 
		-- 	用户邮箱-Email  用户手机号phong  用户简历-resume

		--      news：包含以下字段
		-- 	新闻id  新闻标题  新闻概要  新闻内容  新闻发布事件  点击数量  收藏数量


        -- 新建一个 czxy 的数据库
        create database czxy;
        -- 字符集编码设置
        alter database czxy charset=utf8;
        -- 选中czxy数据库
        use czxy;
        -- 创建两张表
        create table czxy.user(id int(255), username varchar(255), sex varchar(2)) charset=utf8;
        create table czxy.news(Email varchar(10), phone int(10), resume varchar(255)) charset=utf8;
        -- 给news添加 新闻id  新闻标题  新闻概要  新闻内容  新闻发布事件  点击数量  收藏数量 字段
        alter table news add newsid int(255) first;
        alter table news add newstitle varchar(255);
        alter table news add newssummary varchar(255);
        alter table news add newsconnection varchar(255);
        alter table news add newsthing varchar(255);
        alter table news add clicknumber varchar(255);
        alter table news add collectnumber varchar(255);
        alter table news add charset varchar(255);

        -- 分别使用navcat 和 命令行对以上数据库插入两条数据
        INSERT INTO news
        (id, phone)
        VALUES
        ("1", "13333333333");
        -- 删除表中的任意一条数据
        alter table user drop id;