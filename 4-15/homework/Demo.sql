-- 1新建表
create table student(
    no varchar(20) primary key not null comment '学号',
    name varchar(20) not null comment '学生姓名',
    sex varchar(20) not null comment '学生性别',
    birthday datetime comment '学生出生年月',
    class varchar(20) comment '学生所在班级s'
);

create table course(
    no varchar(20) not null comment '课程号',
    name varchar(20) not null comment '课程名称',
    FOREIGN KEY (t_no) REFERENCES teacher(no)
) 

create table score(
    FOREIGN KEY (s_no) REFERENCES student(no),
    FOREIGN KEY (c_no) REFERENCES course(no),
    degree Decimal(4,1) comment '成绩'
)

create table teacher(
    no varchar(20) primary key not null comment '教工编号',
    name varchar(20) not null comment '教工姓名',
    sex varchar(20) not null comment '教工性别',
    birthday datetime comment '教工出生年月',
    prof varchar(20) comment '职称',
    depart varchar(20) not null '教工所在部门'
)

-- 插入数据学生表
insert into student values
('101','赵军','男','1987-03-20 00:0','95033'),
('103','毛军','男','1984-09-03 00:0','95031'),
('105','李明','男','1982-11-02 00:0','95031'),
('107','范丽','女','1987-01-23 00:0','95033'),
('108','王华','男','1981-09-01 00:0','95033'),
('109','张芳','女','1983-01-10 00:0','95031');

-- 插入老师表数据
insert into teacher values
('804','王诚','男','1957-12-02 00:0','副教授','计算机系'),
('825','张萍','女','1971-05-05 00:0','助教','计算机系'),
('831','毛冰','女','1975-08-14 00:0','助教','电子工程系'),
('856','李旭','男','1966-03-12 00:0','讲师','电子工程系');

-- 插入课程表数据
insert into course values
('3-105','计算机导论','825'),
('3-245','操作系统','804'),
('6-166','数字电路','856'),
('9-888','高等数学','831');

-- 插入分数表数据
insert into score values 
('103','3-105',92),
('103','3-245',86),
('103','6-166',85),
('105','3-105',88),
('105','3-245',75),
('105','6-166',79),
('109','3-105',76),
('109','3-245',68),
('109','6-166',81);

-- 查询student表中的所有记录的name、sex和class列信息
select name,sex,class from student;
-- 查询教师所有的单位(不重复)的depart列信息
select distinct depart from teacher;
-- 查询score表中成绩在70到90之间的所有记录
select * from score where degree between 70 and 90;
-- 查询score表中成绩为68，75或88的记录
select * from score where degree = 68 or degree = 75 or degree = 88 ;
-- 查询student表中“95031”班或性别为“女”的同学记录
select * from student where class = '95031' or sex = '女';
-- 以c_no升序、degree降序查询score表的所有记录
select * from score order by c_no asc,degree desc;
-- 查询“95031”班的学生人数
select count(*) from student;
-- 查询score表中的最高分的学生学号和课程号
select s_no,c_no from (select max(degree),s_no,c_no from score) as info;
-- 查询每门课的平均成绩
select c_no,avg(degree) from score;
-- 查询分数大于60，小于80的s_no列
select s_no from score where degree between 60 and 80;
-- 查询student表中姓“王”的同学记录
select * from student where name like '王%';
-- 查询student表中最大和最小的birthday日期值
select max(birthday),min(birthday) from student;
-- 以年龄从小到大的顺序查询student表中的全部记录
select * from student order by birthday desc;
-- 查询最高分同学的成绩信息
select s_no,c_no,degree from (select max(degree),s_no,c_no,degree from score) as shuai;
-- 把学号为‘108’，名字为‘王华’学生的班级改为‘95034’
update student set class='95034' where no='108'and name='王华';