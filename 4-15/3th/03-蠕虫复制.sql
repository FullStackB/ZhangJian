-- 1. 创建一个旧数据表()
create table old_worm(
    pinyin varchar(2)
);

-- 2. 在旧数据表中插入数据
insert into old_worm values('a'),('b'),('c');

-- 3. 创建一个新的数据表 去复制 旧数据表的结构
create table new_worm like old_worm;

-- 4. 使用蠕虫复制
-- insert into 新表名 [(字段列表)] select */字段列表 from 旧表名;
insert into new_worm select * from old_worm;