-- 1.创建数据表
create table duplicate_key(
  s_id varchar(5) primary key,
  s_name varchar(5) not null
);

-- 2.插入一堆数据
insert into duplicate_key values
('s001', '赵一'),
('s002', '钱二'),
('s003', '孙三'),
('s004', '李四'),
('s005', '王五');


3.插入一个和数据库中数据的主键一样的数据 Duplicate entry 's002' for key 'PRIMARY'（拦截了键入的s002 因为是主键）


-- 4.解决主键冲突
4.1 Insert into 表名 [(字段列表)] values(值列表) on duplicate key update 字段 = 新值;  相当于数据更新

4.2 Replace into 表名 [(字段列表)] values(值列表); (相当于先删除后添加)


