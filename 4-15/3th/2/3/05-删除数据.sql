-- 删除的基本语法: delete from 表名 where 条件;

-- 删除复合条件的限定数量的数据
-- 基本语法: delete from 表名 where 条件 limit 数量;


-- Delete删除数据的时候无法重置auto_increment

-- 因为删除不会重置auto_increment
-- 因此要使用Truncate 表名来重置


-- 重置truncate 表名 比较危险  建议大家 尤其主键 就不用重置