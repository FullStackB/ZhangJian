-- 完整的查询指令
-- Select select选项 字段列表 from 数据源 where条件 group by分组 having条件 order by排序 limit限制;


-- 查询非重复数据(原因就是 要所有字段都必须一致才认为是重复数据)
select distinct * from 表;

基本语法：字段名 [as] 别名

-- select distinct 字段名 as name1, 字段名  as name2 from 表名;

-- 多表查询
select * from 表1, 表2;


