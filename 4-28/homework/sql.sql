-- 创建商品表
create table product(
  pid  varchar(50) ,   -- 商品id
  pname varchar(50),   -- 商品名称
  pirce double ,       -- 商品价格
  stock  int,           -- 库存量
   PRIMARY KEY (`pid`) 
); 

-- 商品表数据
insert into product value("p001","计算机",5000,20);
insert into product value("p002","空调",2400,60);
insert into product value("p003","冰箱",1500,20);
insert into product value("p004","沙发",800,10);
insert into product value("p005","打印机",1100,50);

-- 创建顾客表
create table  customer( 
  cid  varchar(50),    -- 顾客id
  cname varchar(50),   -- 顾客姓名
  sex  varchar(2) ,    -- 性别 男/女
  age  int,             -- 年龄 
  PRIMARY KEY (`cid`)  
);
-- 顾客表数据
insert into customer value("c001","刘二","男",43);
insert into customer value("c002","张三","男",28);
insert into customer value("c003","李四","女",30);
insert into customer value("c004","王五","男",20);
insert into customer value("c005","赵六","男",50);
insert into customer value("c006","丽丽","女",50);

-- 创建订单表
create table orderItem(
cid  varchar(50),      -- 顾客id
pid  varchar(50) , -- 商品id
count  int,            -- 商品数量
ordertime  datetime    -- 下单时间
);
-- 订单表数据
insert into orderitem value("c001","p001",5,"2018-4-1" );
insert into orderitem value("c001","p002",2,"2018-4-7" );
insert into orderitem value("c001","p004",2,"2018-4-7" );
insert into orderitem value("c002","p003",10,"2018-5-1" );
insert into orderitem value("c002","p005",5,"2018-5-1" );
insert into orderitem value("c003","p004",5,"2018-5-10" );
insert into orderitem value("c004","p001",5,"2018-5-10" );
insert into orderitem value("c004","p002",2,"2018-5-1" );
insert into orderitem value("c005","p003",10,"2018-5-1" );


-- 单表基础查询
-- 1)	统计订购了商品的总人数。
select count(cid) 总人数 from orderitem;
-- 2)	统计顾客号和所订购商品总数量
select cid,sum(count) from orderitem group by cid order by count asc;
 
-- 子查询：
-- 1)	查找没订购商品的顾客号和顾客名。
select c.cid,c.cname from customer c where cid not in(select cid from orderitem);
-- 2)	查找订购商品号'p001'商品数量最多的顾客号和顾客名
select c.cid,c.cname from product p,orderitem o,customer c where c.cid = o.cid and o.pid = p.pid and p.pid = 'p001' order by count desc limit 1;
-- 3)	统计至少订购2种商品的顾客id和顾客名。
select cid,cname from customer where cid in(select cid from orderitem group by cid having count(cid)>1);
-- 自连接
-- 左右连接
-- 1)	查找所有顾客号和顾客名以及他们购买的商品号
select o.cid,c.cname,o.pid from orderitem o,customer c where o.cid = c.cid;
-- 函数应用
-- 多表查询
-- 1)	查找订购了商品"p001"的顾客号和顾客名。
select o.cid,c.cname,o.pid from orderitem o,customer c where o.cid = c.cid and o.pid = 'p001';
-- 2)	查找订购了商品号为"p001"或者"p002"的顾客号和顾客名。
select c.cid,c.cname,o.pid from orderitem o,customer c where o.cid = c.cid and o.pid = 'p001' or o.pid = 'p002'  group by cid order by pid asc;
-- 3)	查找年龄在30至40岁的顾客所购买的商品名及商品单价。
select c.cname,p.pid,p.pirce from product p,orderitem o,customer c where o.cid = c.cid and o.pid = p.pid and c.age = '30' or c.age = '40';
-- 4)	查找女顾客购买的商品号，商品名和价格。
select c.cname,p.pid,p.pname,p.pirce from product p,orderitem o,customer c where o.cid = c.cid and o.pid = p.pid and c.sex = '女';