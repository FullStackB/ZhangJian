const express = require('express');
const app = express();

app.use(express.static('piblic'));

const route = require('./route');

app.use(route);

const ejs = require('ejs');

app.set('view engine','ejs');

app.set('views','./views');

app.listen(80, () => {
    console.log("sever is running at http://localhost");
})