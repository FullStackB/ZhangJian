// 后台登录页面
module.exports.beLogin = (req,res) => {
    res.render('login');
}
// 后台首页
module.exports.beIndex = (req,res) => {
    res.render('index');
}

// 文章管理页面
module.exports.bePoste = (req,res) => {
    res.render('posts');
}

// 文章的新增
module.exports.beAddPosts = (req,res) => {
    res.render('post-add');
}

// 分类目录管理
module.exports.beCategories = (req,res) => {
    res.render('categories');
}

// 评论管理
module.exports.beComments = (req,res) => {
    res.render('comments');
}
// 用户管理
module.exports.beUser = (req,res) => {
    res.render('users');
}

// 导航设置
module.exports.beNav = (req,res) => {
    res.render('nav-menus');
}
// 轮播图管理
module.exports.beSlides = (req,res) => {
    res.render('slider');
}

//网站管理 
module.exports.beSettings = (req,res) => {
    res.render('settings');
}

// 个人中心
module.exports.beprofile = (req,res) => {
    res.render('profile');
}

// //前台首页
// module.exports.beNav = (req, res) => {
//   res.render('nav-menus');
// }

// // 前台列表
// module.exports.beNav = (req, res) => {
//   res.render('nav-menus');
// }

// // 详细文章
// module.exports.beNav = (req, res) => {
//   res.render('nav-menus');
// }
