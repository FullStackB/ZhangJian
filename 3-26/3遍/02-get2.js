//  导包
const  express = require('express');
// 使用express创建服务器
const app = express();
// 设置静态资源目录
app.use(express.static('public'));
// 当浏览器请求/login的时候服务器返回一下浏览器在表单中输入的内容s
app.post('/login', (req, res) => {
    console.log(req.body);
    res.send(req.query);
})
// 启动服务指定端口
app.listen(80, () => {
    console.log("服务器运行中")
})