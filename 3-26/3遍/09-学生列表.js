// 学生的对象
let students = [
    {
        name: 'wo',
        age: '?',
        sex: '?',
        hobby: 'mhw'
    },{
        name: 'me',
        age: '∞',
        sex: '?',
        hobby: 'ds3'
    },{
        name: 'I',
        age: 'zero',
        sex: "zero",
        hobby: 'dota2'
    }
]

// 获取包
const http = require('http');
const template = require('art-template');
const path = require('path');
// 用包
const app = http.createServer();

app.on('request', (req, res) => {
    let html = template(path.join(__dirname, './public/student.html'), {students: students});
    res.end(html);
})

// 监听端口
app.listen(80, () => {
    console.log('服务器运行中');
})