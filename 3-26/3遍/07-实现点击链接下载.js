// 引入express包
const express = require('express');
const fs = require('fs');
const path = require('path');
// 使用express创建服务器
const app = express();
// 设置静态资源目录
app.get('/', (req, res) => {
    res.download(path.join(__dirname, './download/Yes - Roundabout.mp3'), 'Roundabout.mp3', (error) => {
        if (error) {
            console.log(error);
        }
        console.log('compelte');
    })
})
// 启动服务指定端口
app.listen(80, () => {
    console.log("http://127.0.0.1")
})