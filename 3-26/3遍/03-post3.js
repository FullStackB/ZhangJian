// 引入express包
const express = require('express');
const bodyParser = require('body-parser');
// 使用express 创建服务器
const app = express();
// 使用urlendoded解析前端传来的数据 会把数据解析成字符串或数组
app.use(bodyParser.urlencoded({ extend: false }));
// 设置静态资源目录
app.use(express.static("public"));
// 当浏览器使用post方式请求/login的时候返回一个响应
app.post('/login', (req, res) => {
    console.log(req.body);
    res.send(req.body);
})
// 启动服务器指定端口
app.listen(80, () => {
    console.log("http://127.0.0.1")
})