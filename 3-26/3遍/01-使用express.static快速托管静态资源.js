// 导包
const express = require('express');
// 创建服务器
const app = express();

// 3.设置静态资源目录
app.use('/public', express.static('./public'));

app.listen(80, () => {
    console.log("服务器运行中...1");
})