// 引入express包
const express = require('express');
const fs = require('fs');
const path = require('path');
// 使用express创建服务器
const app = express();

// 设置静态资源目录
app.get('/', (req, res) => {
    // send的三种用法
    // 1 发送字符串给浏览器
    // res.send('来吧');
    // 2 发送对象或数组给浏览器
    // res.send(['a', 'b', 'c']);
    // res.send({ name: "jerry", age: 18})
    // 3 发送buffer 这种二进制数据给浏览器
    fs.readFile(path.join(__dirname, "./public/index.html"), (err, data) => {
        if(error) {
            console.log(error);
        }
        res.send(data);
    })
})

// 启动服务指定端口
app.listen(80, () => {
    console.log("http://127.0.0.1");
})