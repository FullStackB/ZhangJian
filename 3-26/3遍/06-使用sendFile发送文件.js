// 引入express包
const express = require('express');
const pathh = require('path');
// 使用express创建服务器
const app = express();
// 设置静态资源目录
app.get('/', (req, res)  => {
    res.sendFile(path.join(__dirname, './public/index.html'));
})
// 启动服务指定端口
app.listen(80, () => {
    console.log("localhost");
})