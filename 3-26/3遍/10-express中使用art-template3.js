// 引包
const express = require('express');

// 用包
const app = express();
// 告诉exprss 我要使用的模板的后缀是html
app.engine('html', require('express-art-template'))
// 设置 模板的路径是views这个路径
app.set('views', './views')
// 学生的对象
let students = [
    {
        name: 'wo',
        age: '?',
        sex: '?',
        hobby: 'mhw'
    }, {
        name: 'me',
        age: '∞',
        sex: '?',
        hobby: 'ds3'
    }, {
        name: 'I',
        age: 'zero',
        sex: "zero",
        hobby: 'dota2'
    }
]

app.get('/', (req, res) => {
    res.render('students.html', { students: students });
})

// 监听端口
app.listen(80, () => {
    console.log('服务器运行中');
})
// 1下载
// npm install --save art-template
// npm install --save express-art-template
// 2配置模板的后缀
// app.engine('html', require('express-art-template'));
// 3配置模板所在的路径
// app.set('views', './views') 第一个views代表的是模板(不能改)  第二个view代表的是模板的路径
// 4模板和数据拼接起来
// res.render('students.html', { students: students });
// res.render('模板名称', 数据对象(必须是对象));