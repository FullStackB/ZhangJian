import Vue from "vue";
import App from "./App.vue";
import router from "./router";

Vue.config.productionTip = false;

// 配置elementUI
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
Vue.use(ElementUI);

// 引入全局基础css
import "./assets/css/base.css";

// 配置axios
import Axios from "axios";
Vue.prototype.$http = Axios;
Axios.defaults.baseURL = "https://www.liulongbin.top:8888/api/private/v1/";

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
