import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      redirect: "/home"
    },
    {
      path: "/home",
      component: Home
    },
    {
      path: "/login",
      name: "about",
      // 当访问 /login的时候 采取引入Login组件
      component: () => import("./views/Login.vue")
    }
  ]
});

// 路由导航守卫
router.beforeEach((to, from, next) => {
  const tokenStr = window.sessionStorage.getItem("token")
  if (to.path == "/login") return next()
  if (!tokenStr) {
    window.sessionStorage.clear()
    return next("/login")
  }
})

export default router;
