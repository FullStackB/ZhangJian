import Vue from 'vue';

import VueRouter from 'vue-router';

Vue.use(VueRouter);

// 引入组件
import app from './app.vue';
import home from './home.vue';
import login from './login.vue';

const router = new VueRouter({
  routes: [
    // 重定向
    {
      path: '/',
      redirect: '/home'
    },
    // 首页
    {
      path: '/home',
      component: home
    },
    // 登录
    {
      path: '/login',
      component: login
    }
  ]
})


// 不登录不能到首页 直接写路由如果没有成功登录过应该也不能去首页 自动跳转到登录页
// 相当于ajax中的beforeSend


// 路由导航守卫 作用是 在加载组件之前 进行校验 如果要加载某个路由 必须通过路由导航守卫放行(校验)
router.beforeEach((to, from, next) => {
  // to 要去哪里
  if (to.path == '/login') return next();
  // from 从哪里来
  // next 放行 下一个动作
  // 在这里要进行token的校验 如果没有token 代表 没有登录或登录失败
  let tokenStr = window.sessionStorage.getItem('token');
  if (!tokenStr) return next('/login');

  next();
})


const vm = new Vue({
  el: '#app',
  router,
  render: h => h(app)
})