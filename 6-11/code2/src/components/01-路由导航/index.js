import Vue from 'vue';

import VueRoute from 'vue-router';

Vue.use(VueRoute);

// 引入组件
import app from './app.vue';
import home from './home.vue';
import login from './login.vue';

const router = new VueRoute({
    routes: [
        {
            path: '/',
            redirect: '/home'
        },
        {
            path: '/home',
            component: home
        },
        {
            path: '/login',
            component: login
        }
    ]
})

// 
router.beforeEach((to,from,next) => {
    if (to.path == '/login') return next();
    let tokenStr = window.sessionStorage.getItem('token');
    if (!tokenStr) return next('/login');
    next();
})

const vm = new Vue({
    el: '#app',
    router,
    render: h => h(app)
})