--  创建学生表字段 id ，name，age，sex，hometown（故乡），tel，room_num(宿舍号：)
create table czxy.stu(id int(255), name varchar(255), age int(3), sex varchar(2), hometown varchar(255), tel varchar(255), room_num int(3));
-- 添加5条数据
insert into stu(id, name, age, sex, hometown, tel, room_num)values{('id', 'name', 'age', 'sex', 'hometown', 'tel', 'room-num'),('id', 'name', 'age', 'sex', 'hometown', 'tel', 'room-num'),('id', 'name', 'age', 'sex', 'hometown', 'tel', 'room-num'),('id', 'name', 'age', 'sex', 'hometown', 'tel', 'room-num'),('id', 'name', 'age', 'sex', 'hometown', 'tel', 'room-num')};

-- 查询故乡是山东的同学信息
select from stu where hometown="山东";

-- 查询哪些人在201宿舍
select name from stu where room_num="201";

-- 查询男生有哪些？
select sex from stu where sex="男";

-- 把手机号码是xxx的性别更新为女
updata stu set sex ="女" where tel ="xxx";

-- 删除id为10和15的同学
delete from stu where id=10;
delete from stu where id=15;