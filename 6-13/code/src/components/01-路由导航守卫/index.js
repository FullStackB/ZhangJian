// 登录之后 才能去首页 如果没有登录 那么 无论是自己点击链接过去的还是自己直接输入网址过去的 都要拦截 跳转到登录页进行登录

// 1.基础工作
import Vue from 'vue';
// 引入路由包
import VueRouter from 'vue-router';
// 使用路由
Vue.use(VueRouter);
// 2.渲染根组件
import App from './app.vue';
// 引入home组件
import Home from './home.vue';
// 引入login组件
import Login from './login.vue';
// 3.写路由
const router = new VueRouter({
  routes: [
    // 重定向
    {
      path: '/',
      redirect: '/home'
    },
    // home路由
    {
      path: '/home',
      component: Home
    },
    // login路由
    {
      path: '/login',
      component: Login
    }

  ]
})
// 4.登录和首页渲染出来
// 5.路由导航守卫
// router.beforeEach((to, from, next) => {
//   // 这里处理代码是空的,但是路由是http://localhost:8080/#/home 在路由规则中 看到/home vue就要执行 加载Home组件
//   // 但是路由导航守卫是空的 就是虽然你要加载Home组件 但是我守在这里 vue也没有告诉我怎么做 我啥都不做 就挡着你
// })


router.beforeEach((to, from, next) => {

  if (to.path === '/login') return next();
  // 如果发现 没有令牌 你不能过去 只能去领令牌
  let tokenStr = window.sessionStorage.getItem('token');
  if (!tokenStr) return next('/login');

  // 放行
  next();
})



// 实例化 Vue
const vm = new Vue({
  el: '#app',
  // 渲染主组件
  render: h => h(App),
  // 挂载路由
  router
})