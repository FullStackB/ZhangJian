import Vue from 'vue';


import app from './app.vue';

// 全局自定义指令  
//  指令已经用过好多了 比如v-for v-if v-show  就是vue提前给我定义好的具有特殊功能的自定义属性
// 我们也想实现指令的效果 怎么办呢 就需要自定义指令
// 语法: Vue.directive(自定义指令的名字, {自定义指令的配置项})

// 需求: 当加了我们自定义的指令之后 让文字变成红色
// vue中 指令都有一个前缀  v- 但是在自定义指令的时候 不用加 之后用的时候 自己加上就可以了
// Vue.directive('red', {
//   bind: function (el) {
//     el.style.color = "red";
//   }
// })

// Vue.directive('blue', {
//   bind: function (el) {
//     el.style.color = "blue";
//   }
// })


// v-show="值" 在vue的指令中 很少有直接把指令放在标签中就能用的  大多数指令都要传参

Vue.directive('color', {
  bind: function (el, binding) {
    // el 代表的是 添加这个指令的元素
    // bingding 代表 v-color=后面的数据 
    el.style.color = binding.value;
    // console.log(binding)
  }
})


// 难道指令只能改变样式吗? 也不是  除了处理样式 还可以处理行为
// 当页面打开的时候 自动获取焦点
// 如何使用js获取input的焦点 .focus();
Vue.directive('focus', {
  // bing 其实是不能用来实现js行为的
  // 推荐使用bind来处理样式
  bind: function (el, binding) {
    el.focus();
    console.log(el.parentNode);
  },
  // inserted 用来执行js
  // 推荐使用inserted处理js
  inserted: function (el) {
    el.focus();
    console.log(el.parentNode + "aaa");
  }
})




const vm = new Vue({
  el: '#app',
  render: h => h(app)
})