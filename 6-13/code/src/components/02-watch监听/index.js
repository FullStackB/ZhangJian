import Vue from 'vue';

import VueRouter from 'vue-router';

Vue.use(VueRouter);

import app from './app.vue';
import login from './login.vue';


const router = new VueRouter({
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      component: login
    }
  ]
})


const vm = new Vue({
  el: '#app',
  render: h => h(app),
  router
})