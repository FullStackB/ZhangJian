// 1.引入vue
import Vue from 'vue';
// 2.引入vue-router
import VueRouter from 'vue-router';
// 3.让vue使用vue-router
Vue.use(VueRouter);
// 4.引入组件
import app from './app.vue';
import home from './home.vue';
import about from './about.vue';
import movie from './movie.vue';

import inner from './tabs/inner.vue';
import outer from './tabs/outer.vue';

// 5.配置路由规则
const router = new VueRouter({
  routes: [
    // 当要设置默认的地址的时候 使用重定向
    { path: '/', redirect: '/home' },
    // 当请求 #/home 的时候 展示 home组件

    { path: '/home', component: home },
    { path: '/about', component: about },
    {
      path: '/movie', component: movie, redirect: '/movie/inner',
      children: [
        // { path: '/movie', redirect: '/movie/inner' }, 这是在设置默认的路由
        {
          path: '/movie/inner',
          component: inner
        },
        {
          path: '/movie/outer',
          component: outer
        }
      ]
    },
  ],
  linkActiveClass: 'active'
})

// 6.实例化vue 渲染主组件 挂载路由 
const vm = new Vue({
  router,
  render: h => h(app)
}).$mount('#app');