import Vue from 'vue';

import VueRouter from 'vue-router';

Vue.use(VueRouter);

import app from './app.vue';
import movieList from './movieList.vue';
import movieDetail from './movieDetail.vue';


const router = new VueRouter({
  routes: [
    {
      path: '/',
      redirect: '/movie/list'
    },
    {
      path: '/movie/list',
      component: movieList
    },
    {
      // :id就是一个占位符 为了以后进行替换
      // /movie/detail/:id  以后会变成 /movie/detail/5
      path: '/movie/detail/:id',
      component: movieDetail,
      // 开启路由传参
      props: true
    },
  ]
})


const vm = new Vue({
  el: '#app',
  render: h => h(app),
  router
})


// 路由传参的推荐方式
// 1.设置路由  path: '/movie/detail/:id', :id 为占位符
// 2.在和path同级  配置 props为true
// 3.在你要用的那个组件中 使用props数组 元素为 id 要一样 接受
// 4.在组件中使用this.id 用就可以了 但是一般都要把前一个组件传来的值存一下 所以写一个data再用