// 1.引入vue
import Vue from 'vue';
// 2.引入vue-router
import VueRouter from 'vue-router';
// 3.让vue使用vue-router
Vue.use(VueRouter);
// 4.引入组件
import app from './app.vue';
import movieList from './movieList.vue'
import movieDetail from './movieDetail.vue';

const router = new VueRouter({
  routes: [
    {
      path: '/',
      redirect: '/movie/list'
    },
    {
      path: '/movie/list',
      component: movieList
    },
    {
      path: '/movie/detail/:id',
      component: movieDetail,
      props: true
    },
  ]
})

// 6.实例化vue 渲染主组件 挂载路由 
const vm = new Vue({
  router,
  render: h => h(app)
}).$mount('#app');