const webpack = require('webpack');

module.exports = {
    devServer: {
        open: true,
        hot: true
    }
}