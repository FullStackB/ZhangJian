const faker = require('faker');
const _ = require('lodash');
faker.locale = 'zh_CN';
module.exports = () => {
    const data = {
        brandlist:
            [
                {
                    "id": 1,
                    "brandName": "阿拉拉了",
                    "brandTime": new Date()
                },
                {
                    "id": 2,
                    "brandName": "安踏品牌",
                    "brandTime": new Date()
                }
            ]
    }
    return data
}