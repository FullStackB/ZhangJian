import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui'
import './assets/fonts/iconfont.css'
import 'element-ui/lib/theme-chalk/index.css'
import './assets/css/index.css'
//使用axios
import axios from 'axios'
//使用axios默认使用url路由
axios.defaults.baseURL = 'http://localhost:3000'
Vue.prototype.$http = axios
Vue.use(ElementUI)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
