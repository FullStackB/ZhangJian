0.显示用户登录页面
1.用户登录
  1.1 点击登录按钮
  1.2 收集表单信息
  1.3 发起aja请求
  1.4 路由
  1.5 控制器
  1.6 sql语句
2.cookie
  2.1 什么是cookie
    是一个客户端存储 cookie 曲奇饼干
  2.2 cookie的特点
    不安全
    大小只有4kb
    只能存储字符串
  2.3 cookie的使用
    在服务器创建
      res.cookie(key,value)
    验证cookie
      req.headers.cookie
    在浏览器怎么查看cookie
      在f12工具中的application 中 cookie
3.session
    3.1 session是什么
        session是基于cookie的存储数据的工具
    3.2 session的特点
        安全 
        没有存储限制
        除了资源之外什么都可以存
    3.3 session的使用
      3.3.1 下载包 express-session
      3.3.2 引入express-session  放在const app=express();下面
      3.3.3 配置
        app.use(session({
          secret: 'keyboard cat',
          resave: false,
          saveUninitialized: flase,
          cookie: {}
          }))
      3.3.4 设置session
          req.session.键 = 值;
      3.3.5 验证session
          req.session.键=值
4.把cookie和session用到项目中