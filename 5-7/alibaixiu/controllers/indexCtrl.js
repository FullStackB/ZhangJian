// 显示首页
module.exports.index = (req, res) => {
  // console.log(req.headers);
  // console.log(req.session.isLogin);
  if (!req.session.isLogin) {
    res.redirect('/login');
    return;
  }

  res.render('index');
};