// 引入数据库连接
const conn = require('../data');

// 用户管理
module.exports.Users = (req, res) => {
  res.render('users');
}

// 显示所有用户
module.exports.usersFind = (req, res) => {
  conn.query('select * from users', (err, results) => {
    if (err) {
      return console.log(err);
    }

    res.json(results);
  })
}

// 添加用户
module.exports.userAdd = (req, res) => {
  console.log(req.body);
  let sqlDate = ['/uploads/avatar_1.jpg', req.body.email, req.body.slug, req.body.nickname, req.body.password, 'activated'];
  let sql = 'insert into users values (null, ?,?,?,?,?,?);'

  conn.query(sql, sqlDate, (error, results) => {
    if (error) {
      return console.log(error);
    }

    if (results.affectedRows) {
      res.json({
        code: '1000',
        message: '用户添加成功'
      })
    }
  })
}

// 删除用户
module.exports.userDelete = (req, res) => {
  // console.log(req.query.id);
  conn.query('delete from users where id=?', req.query.id, (error, results) => {
    if (error) {
      return console.log(error);
    }

    if (results.affectedRows) {
      res.json({
        code: '1001',
        message: '用户删除成功'
      })
    }
  })
}


// 根据id查询用户
module.exports.userFind = (req, res) => {
  // console.log(req.query);
  conn.query('select * from users where id =?', [req.query.id], (error, results) => {
    if (error) return console.log(error);

    // console.log(results);
    res.json(results);
  })
}

// 根据id更新用户信息
module.exports.userUpdate = (req, res) => {
  let sql = 'update users set email=?, slug=?, nickname=?,password=? where id=?';
  let params = [req.body.email, req.body.slug, req.body.nickname, req.body.password, req.body.id];
  // console.log(req.body);
  conn.query(sql, params, (error, results) => {
    if (error) return console.log(error);

    // console.log(results);
    if (results.affectedRows) {
      res.json({
        code: '1002',
        message: '用户信息更新成功'
      })
    }
  })
}

// 批量删除多个
module.exports.usersDelete = (req, res) => {
  // console.log(req.query);
  conn.query('delete from users where id in (?)', [req.query.idArr], (error, results) => {
    if (error) return console.log(error);

    // console.log(results);
    if (results.affectedRows) {
      res.json({
        code: '1001',
        message: '用户删除成功'
      })
    }
  })
}