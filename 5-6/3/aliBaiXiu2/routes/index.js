// 1.引入express
const express = require('express');
// 2.使用express的Router来创建路由
const router = express.Router();
// 3.引入控制器模块
const controller = require('../controllers');
// 4.创建路由规则(前台使用fe  后台使用be)

// 4.1 配置登录页显示
router.get('/belogin', controller.beLogin);

// 4.2 配置首页显示
router.get('/beindex', controller.beIndex);

// 4.3 配置文章管理页面
router.get('/beposts', controller.bePosts);
// 4.4 配置增加(写)文章页面
router.get('/beaddposts', controller.beAddPosts);
// 4.5 配置分类目录页面
router.get('/becategories', controller.beCategories);
// 4.6 配置评论管理页面
router.get('/becomments', controller.beComments);
// 4.7 配置用户管理页面
router.get('/beusers', controller.beUsers);
// 4.7.1 用户查询
router.get('/usersFind', controller.usersFind);
// 4.7.2 用户添加
router.post('/userAdd', controller.userAdd);
// 4.7.3 用户删除
router.get('/usersDelete', controller.usersDelete);
// 4.8 配置导航设置页面
router.get('/benav', controller.beNav);
// 4.9 配置图片轮播管理页面
router.get('/beslides', controller.beSlides);
// 4.10 网站设置页面
router.get('/besettings', controller.beSettings);
// 4.11 个人中心
router.get('/beprofile', controller.beProfile)
// 4.11 前台 首页显示
// router.get('/', controller.index);
// // 4.12 前台 列表页面显示
// router.get('/list', controller.list);
// // 4.13 前台  详细文章显示
// router.get('/details', controller.details);


// 5.暴露路由规则
module.exports = router;