-- 建学生信息表student
create table student(
sno varchar(20) not null primary key,
sname varchar(20) not null,
ssex varchar(20) not null,
sbirthday datetime,
class varchar(20)
);


-- #建立教师表
create table teacher
(
tno varchar(20) not null primary key,
tname varchar(20) not null,
tsex varchar(20) not null,
tbirthday datetime,
prof varchar(20),
depart varchar(20) not null
);



-- #建立课程表course
create table course
(
cno varchar(20) not null primary key,
cname varchar(20) not null,
tno varchar(20) not null,
foreign key(tno) references teacher(tno)
);



-- #建立成绩表
create table score
(
sno varchar(20) not null ,
cno varchar(20) not null,
degree decimal
);



-- #添加学生信息
insert into student values('108','曾华','男','1977-09-01','95033');
insert into student values('105','匡明','男','1975-10-02','95031');
insert into student values('107','王丽','女','1976-01-23','95033');
insert into student values('101','李军','男','1976-02-20','95033');
insert into student values('109','王芳','女','1975-02-10','95031');
insert into student values('103','陆君','男','1974-06-03','95031');


-- #添加教师表
insert into teacher values('804','李诚','男','1958-12-02','副教授','计算机系');
insert into teacher values('856','张旭','男','1969-03-12','讲师','电子工程系');
insert into teacher values('825','王萍','女','1972-05-05','助教','计算机系');
insert into teacher values('831','刘冰','女','1977-08-14','助教','电子工程系');



-- #添加课程表
insert into course values('3-105','计算机导论','825');
insert into course values('3-245','操作系统','804');
insert into course values('6-166','数字电路','856');
insert into course values('9-888','高等数学','831');




-- #添加成绩表
insert into score values('103','3-245','86');
insert into score values('105','3-245','75');
insert into score values('109','3-245','68');
insert into score values('103','3-105','92');
insert into score values('105','3-105','88');
insert into score values('109','3-105','76');
insert into score values('103','3-105','64');
insert into score values('105','3-105','91');
insert into score values('109','3-105','78');
insert into score values('103','6-166','85');
insert into score values('105','6-166','79');
insert into score values('109','6-166','81');

-- 1.	查询教师所有的单位即不重复的Depart列。
select distinct depart from teacher;
-- 2、 查询Student表中“95031”班或性别为“女”的同学记录。
select * from student class = '95031' or ssex = '女';
-- 3.以Class降序查询Student表的所有记录。
select * from student order by asc;
-- 4.	以Cno升序、Degree降序查询Score表的所有记录。
select * from score order by degree desc,cno asc;
-- 5.	查询Score表中至少有5名学生选修的并以3开头的课程的平均分数。
select avg(Degree ) from Score where Cno like '3%' group by Cno having count(Cno)>4;
-- 6.查询所有学生的Sname、Cno和Degree列。
select s.sname,sc.cno,sc.degree from student s,score sc where s.sno = sc.sno;
-- 7.查询“张旭“教师任课的学生成绩。
select Degree from Score,Teacher,Course where Teacher.Tname='张旭' and Teacher.Tno=Course.Tno and Course.Cno=Score.Cno;
-- 8.	查询所有教师和同学的name、sex和birthday。
select s.sname,s.ssex,s.sbirthday,t.tname,t.tsex,t.tbirthday from student s,teacher t;
-- 9.	查询所有“女”教师和“女”同学的name、sex和birthday。
select s.sname,s.ssex,s.sbirthday,t.tname,t.tsex,t.tbirthday from student s,teacher t  where s.ssex = '女' or t.tsex = '女';
-- 10.查询成绩比该课程平均成绩低的同学的信息。
select Sno,Cno,Degree from Score a where a.Degree<(select AVG(Degree) from Score b where a.Cno=b.Cno);
-- 10.	查询所有任课教师的Tname和Depart。
select Tname,Depart from Teacher where tno in (select tno from course where Cno in (select distinct Cno from Score));
-- 11.	查询Student表中不姓“王”的同学记录。
select * from student where Sname not like '王%';
-- 12.	查询至少有2名男生的班号。
select Class FROM	student where Ssex='男' group by Class having COUNT(*)>1;
-- 13.	查询Student表中最大和最小的Sbirthday日期值。
select MAX(Sbirthday) as 最大,MIN(Sbirthday) as 最小 from student;
-- 14.	查询最高分同学的Sno、Cno和Degree列。
select Sno,Cno,Degree from Score where degree=(select MAX(Degree)from Score);
-- 15.	查询和“李军”同性别的所有同学的Sname。
select Sname from student where Ssex=(select Ssex from student where Sname='李军') and Sname not in ('李军');

-- 16.	查询所有选修“计算机导论”课程的“男”同学的成绩表。
select Sno,Degree from Score where Sno in (select Sno from student where Ssex='男') and Cno in (select Cno from Course where Cname='计算机导论');