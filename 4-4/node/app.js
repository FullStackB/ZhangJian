// 导入express框架
let express = require("express");

// 初始化express
let app = express();

// 处理post请求
const bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({ extended: false }));

// 设置模板引擎相关信息
let ejs = require("ejs");
let path = require("path");

// 设置模板的存放目录
// 第一个参数：固定的
// 第二个参数：模板存在的目录
app.set("views", './views');

// 定义使用的模板引擎
// 第一个参数：模板引擎的名称，模板引擎的后缀
// 第二个参数：使用模板引擎的方法
app.engine("html", ejs.__express);

// 在app中注册模板引擎
// 第一个参数：固定不变
// 第二个参数：与定义的模板引擎的名称有关
app.set("view engine", "html");


// 设置静态资源的访问
app.use("/public", express.static(__dirname + "/public"));

// 导入后台的路由文件
let adminRouter = require("./routers/admin");

// 使用前台路由
// 参数1：匹配路由规则
// 参数2：请求路由规则
app.use('/', adminRouter);

// 监听服务器
app.listen(7788, function () {
	console.log("node 服务器已启动 端口7788");
});