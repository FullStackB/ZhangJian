// 导入express
let express = require("express");

// 实例化
let router = express.Router();
const crypto = require('crypto');
const mysql = require("../config/db.js");
// 监听用户的访问地址

// 后台首页路由
router.get("/",function(req,res,next){
	// 加载对应后台页面
	res.render("admin/index");
});

// 后台欢迎页面
router.get("/welcome",function(req,res,next){
	// 加载对应欢迎页面
	res.render("admin/welcome");
});

// 管理员管理
let adminRouter = require('./admin/admin');
router.use('/admin',adminRouter);

module.exports = router;