// 引包
const express = require('express');
const fs = require('fs');
const path = require('path');
const mysql = require('mysql');
// 连接服务器
const connection = mysql.createConnection({
    host: '127.0.0.1',
    port: '3306',
    user: 'root',
    password: 'root',
    database: "user"
});
// 转到指定首页
module.exports.index = (req, res) => {
    res.render('index.html');
}
// 添加用户
module.exports.adduser = (req, res) => {
    connection.query('insert into users(username,password) values("' + req.body.username + '","' + req.body.password + '")', (error, result) => {
        if (error) {
            console.log(error);
        } else {
            res.redirect('/show');
        }
    })
};
// 将用户的数据显示到表格中
module.exports.show = (req, res) => {
    connection.query('select * from users', (error, result) => {
        if (error) {
            console.log(error);
        } else {
            res.render('show.html', { list: result });
        }
    })
};

// 删除用户
module.exports.deluser = (req, res) => {
    connection.query('delete from users where id =' + req.query.id, (error, result) => {
        if (error) {
            console.log(error);
        }
    })
    // 页面不跳转
    res.redirect('/show');
}
// 跳转到修改页面
module.exports.gai = (req, res) => {
    console.log(req.query.id);
    var obj = req.query.id;
    console.log(obj);
    var arr = [];
    arr.push(obj);
    console.log(arr);
    res.render('gai.html', { list: arr });
}
// 更改数据并跳转到表格页面
module.exports.update = (req, res) => {
    var od = parseInt(req.body.id);
    console.log(req.body.id);
    connection.query('update users set username = ("' + req.body.username + '"),password=("' + req.body.password + '") where id =("' + od + '")', (error, result) => {
        if (error) {
            console.log(error);
        }
    })
    res.redirect('/show');
}
// 维持修改也删除
module.exports.show1 = (req, res) => {
    connection.query('select * from users', (error, result) => {
        if (error) {
            console.log(error);
        } else {
            res.render('show.html', { list: result });
        }
    })
};