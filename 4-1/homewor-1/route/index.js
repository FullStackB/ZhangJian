// 添加路由express框架
const express = require('express');
// 使用Router创建路由框架
const router = express.Router();
// 调用业务处理模块
const controller = require('../controller');
// 注册
router.get('/',controller.index);
// 添加用户
router.post('/adduser',controller.adduser);
// 展示页面
router.get('/show',controller.show);
// 删除用户
router.get('/deluser',controller.deluser);
router.get('/gai',controller.gai);
router.post('/update',controller.update);
// 把路由暴露出去
module.exports = router
