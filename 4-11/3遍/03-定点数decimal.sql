-- 创建数据表
create table num_deci(
  f1 float(10,2),
  d1 decimal(10,2)
);

-- 先写一个合法的数字
insert into num_deci values(12345678.90,12345678.90);

-- 写一个超出范围的数字

insert into num_deci values(12345678.90,123456789.999);


-- 写一个差不多可以四舍五入的
insert into num_deci values(12345678.90,99999999.99);