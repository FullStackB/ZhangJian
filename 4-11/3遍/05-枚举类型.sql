-- 创建表
create table str_enum(
  sex enum('男','女','保密')
);


-- 添加数据
insert into str_enum values('妖');

-- 枚举类型的存储 
-- 枚举类型存储的方式是 存的 当时设置的 值的下标

select sex +0 from str_enum;

insert into str_enum values(3);