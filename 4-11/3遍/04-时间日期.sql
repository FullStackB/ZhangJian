-- 创建数据表
create table str_date(
  t1 date,
  t2 time,
  t3 datetime,
  t4 timestamp,
  t5 year
);

-- 插入数据
insert into str_date values(
  '1900-01-01',
  '12:12:12',
  '1900-01-01 12:12:12',
  '1970-01-01 12:12:12',
  '99'
);

-- 更新
update str_date set t5='59' where t1='1900-01-01';


-- 年的两种表示
-- 年最好用四位 
-- 两位也行 但是如果年的数字是70及以上 那么 显示位19+年的数字  否则  显示20+年的数字
insert into str_date values(
  '1900-01-01',
  '12:12:12',
  '1900-01-01 12:12:12',
  '1970-01-01 12:12:12',
  '2079'
);