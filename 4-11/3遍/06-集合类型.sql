-- 创建数据表

create table str_set(
  hobby set('篮球','足球','抽烟','耍猴','铁环','中华箭','吃鸡','打麻将')
);


-- 插入爱好数据
insert into str_set values('吃鸡','打麻将','耍猴');

insert into str_set values('吃鸡,打麻将,耍猴');


-- 插入的数据存的是二进制
'篮球','足球','抽烟','耍猴','铁环','中华箭','吃鸡','打麻将'
  1      1      1     1      1       1      1      1
  1      2      4     8      16      32     64     128

-- 查看被选中的值到底存的啥样子 200  = 128 +64++8 = 200;
select hobby +0 from str_set;


-- 我的爱好最近变了 足球  抽烟  耍猴 中华键 46
insert into str_set values(46);