
  int_1 tinyint,
  int_2 smallint,
  int_3 mediumint,
  int_4 int,
  int_5 bigint
);

-- 插入数据
insert into  num_int values(1,100,1000,10000,100000,255,111);


-- zerofill 填充0  

alter table num_int add int_7 tinyint zerofill;


-- 改字符的类型
alter table num_int modify int_7 tinyint(2) zerofill;