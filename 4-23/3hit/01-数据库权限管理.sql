-- 将权限分配给指定的用户
-- 语法： grant 权限列表 on数据库 /*.表名/* to 用户;

-- 权限列表： 使用逗号分隔，但是可以使用all privileges代表全部权限
-- 数据库.表名： 可以是单表  (数据库名字.表名), 可以是具体某个数据库(数据库.*), 也可以整库(*.*)

-- 0.登录 root用户 删除 wanlum用户
drop user 'wanlum'@'localhost';

-- 1. 创建一个哟用户 wanlum
create user 'wanlum'@'localhost' identified by 'wanlum';

-- 2. 登录root用户 因为只有root用户可以给指定的用户分配权限
基本语法： grant 权限列表 on 数据库 /*.表名/* to 用户;

grant select on mysql.user to 'wanlum'@'localhost';

-- 权限回收: 将权限从用户手中收回

-- 基本语法：revoke 权限列表/all privileges on  数据库/*.表名/* from  用户;
revoke select on mysql.user from 'wanlum'@'localhost';