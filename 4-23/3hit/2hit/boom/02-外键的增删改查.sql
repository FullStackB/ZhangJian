-- 增加外键(表后增加)
-- 基本语法： Alter table 从表 add [constraint `外键名`] foreign key(外键字段) references 主表(主键);

-- 查看外键 就是在查看表的创建过成

-- 删除外键/修改外键(先删后加叫修改)
-- 外键不允许修改，只能先删除后增加
-- 基本语法：alter table 从表 drop foreign key 外键名字;