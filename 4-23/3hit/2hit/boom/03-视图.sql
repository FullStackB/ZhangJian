-- 视图的本质是SQL指令(select语句)
-- 基本语法: create view 视图名字 as select指令; //可以是单表数据，也可以是连接查询, 联合查询或子查询

-- 使用视图
-- 基本语法:  select 字段列表 from 视图名字[子句];


-- 修改视图
-- 基本语法: select 字段列表 from 视图名字[子句];

-- 修改视图: 本质是修改视图对应的查询语句
-- 基本语法: alter view 视图名字 as 新select指令;

-- 删除视图
-- drop view 视图的名字

-- 查看视图
-- desc 视图名字;


-- show create view 视图名字