-- 视图的本质是SQL指令(select语句)
-- 基本语法: create view 视图名字 as select指令;   //可以是单表数据, 也可以是连接查询,联合查询或者子查询

create view stu_info_v  as select * from stu_Info;

-- 使用视图
-- 基本语法: select 字段列表 from 视图名字 [子句];

select info_name from stu_info_v;

-- 修改视图: 本质是修改视图对应的查询语句
-- 基本语法: alter view 视图名字 as 新select指令;
alter view stu_info_v as select info_age from stu_Info;

-- 删除视图
-- drop view 视图的名字
drop view stu_info_v;

-- 查看视图
-- desc 视图名字;
desc stu_info_v
-- show create view  视图名字