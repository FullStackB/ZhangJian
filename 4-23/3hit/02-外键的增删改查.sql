-- 学生和班级

-- 1.创建一个数据库
create database day08_b;
-- 2. 使用数据库
use day08_b;
-- 3.创建班级库
create table stu_class(
    class_id int primary key auto_increment,
    class_name varchar(20)
);

-- 4.创建学生表
create table stu_info (
  info_id int primary key auto_increment,
  info_name varchar(10) not null,
  info_age tinyint default 18,
  class_id int,
  foreign key(class_id) references stu_class(class_id)
);


insert into stu_class values
(null,'全栈应用开发');

insert into stu_info values
(null,'郭嘉', 32, 1);

insert into stu_info values
(null,'孔亮', 28, 1);


-- 增加外键(表后增加)
alter table stu_info add constraint `classId` foreign key(class_id) references stu_class(class_id);
-- 查看外键 就是在查看标的创建过程
show create table stu_info;

-- 删除外键/修改外键(先删后加就是了)
-- 外键不允许修改,只能先删除后增加
-- 基本语法:alter table 从表 drop foreign key 外键名字;
alter table stu_info drop foreign key `stu_info_ibfk_1`;