// 引入express包
const express = require('express');
// 创建express服务器
const app = express();
const favicon = require('serve-favicon');
app.use(favicon(__dirname+ '/favicon.ico'));
// 服务器的配置

// 配置body-parser
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended:false}));

// 设置静态资源
app.use(express.static('public'));
// 配置路由
const route = require('./route');

app.use(route);

// 配置模板
const ejs = require('ejs');

app.set('view engine','ejs');
app.set('views','./views');

app.listen(80,() => {
    console.log("http://localhost");
})