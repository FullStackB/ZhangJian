-- 删除数据库中的表
drop table users;
-- 创建数据库
create database mhw;
-- 选择数据库
use mhw;
-- 编码格式
set names utf8;

-- 创建表  unsigned
create table users( 
    id int not null primary key auto_increment comment  'id',
    nickname varchar(25) not null comment '用户名',
    username varchar(20) not null comment '真实姓名',
    email varchar(20) not null comment '电子邮件',
    phone varchar(20)  not null comment '电话',
    region enum('国际关系地区','北京大学','天津大学') not null comment '地区',
    jurisdiction enum('管理员','普通用户','游客') not null comment '权限',
    state enum('正常','禁用') default '正常' not null comment '状态'
);
insert into users values
(null,'ccc','asd','1323101667@qq.com','17553041262','1','1','1'),
(null,'msj','asf','7646321467@qq.com','17553041262','2','2','2'),
(null,'xxx','cff','4567871667@qq.com','17553041262','3','3','1');