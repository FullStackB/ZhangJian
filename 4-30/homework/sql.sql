-- 1、使用SHOW语句找出在服务器上当前存在什么数据库：
show database;

    -- 2、创建一个数据库MYSQLDATA
create database MYSQLDATA;

    -- 3、选择你所创建的数据库
use MYSQLDATA;

    -- 4、查看现在的数据库中存在什么表
show tables;

    -- 5、创建一个数据库表
create table user(
    id int PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255);
);

    -- 6、显示表的结构：
desc data;

    -- 7、往表中插入一条数据
insert into user values(1,'zj');

    -- 8、删除表
drop table user;

    -- 9、清空表

delete from user;

    -- 12、更新表中一条数据
update user set name='jo' where name = 'zj';

-- 题目二、 
-- 1.创建student和score表
CREATE TABLE student (
id INT(10) NOT NULL UNIQUE PRIMARY KEY ,
name VARCHAR(20) NOT NULL ,
sex VARCHAR(4) ,
birth YEAR,
department VARCHAR(20) ,
address VARCHAR(50) 
);
-- 创建score表。SQL代码如下：
CREATE TABLE score (
id INT(10) NOT NULL UNIQUE PRIMARY KEY AUTO_INCREMENT ,
stu_id INT(10) NOT NULL ,
c_name VARCHAR(20) ,
grade INT(10)
);
-- 2.为student表和score表增加记录
-- 向student表插入记录的INSERT语句如下：
INSERT INTO student VALUES( 901,'张老大', '男',1985,'计算机系', '北京市海淀区');
INSERT INTO student VALUES( 902,'张老二', '男',1986,'中文系', '北京市昌平区');
INSERT INTO student VALUES( 903,'张三', '女',1990,'中文系', '湖南省永州市');
INSERT INTO student VALUES( 904,'李四', '男',1990,'英语系', '辽宁省阜新市');
INSERT INTO student VALUES( 905,'王五', '女',1991,'英语系', '福建省厦门市');
INSERT INTO student VALUES( 906,'王六', '男',1988,'计算机系', '湖南省衡阳市');
-- 向score表插入记录的INSERT语句如下：
INSERT INTO score VALUES(NULL,901, '计算机',98);
INSERT INTO score VALUES(NULL,901, '英语', 80);
INSERT INTO score VALUES(NULL,902, '计算机',65);
INSERT INTO score VALUES(NULL,902, '中文',88);
INSERT INTO score VALUES(NULL,903, '中文',95);
INSERT INTO score VALUES(NULL,904, '计算机',70);
INSERT INTO score VALUES(NULL,904, '英语',92);
INSERT INTO score VALUES(NULL,905, '英语',94);
INSERT INTO score VALUES(NULL,906, '计算机',90);
INSERT INTO score VALUES(NULL,906, '英语',85);

-- 3.查询student表的所有记录
 select * from student;

-- 4.查询student表的第2条到4条记录
 select * from student limit 2,2; 

-- 5.从student表查询所有学生的学号（id）、姓名（name）和院系（department）的信息
select id,`name`,department from student;
 
-- 6.从student表中查询计算机系和英语系的学生的信息
select * from student where department='计算机系' or department='英语系';

-- 7.从student表中查询年龄18~22岁的学生信息
select * from student where  2019-birth between  18 and 22;

-- 8.从student表中查询每个院系有多少人 
select department,count(name) '人' from student group by department;

-- 9.从score表中查询每个科目的最高分
select c_name,max(grade) from score group by c_name;
 
-- 10.查询李四的考试科目（c_name）和考试成绩（grade）
 select a.c_name,a.grade from score a,student b where a.stu_id=b.id and b.name='李四';

-- 11.用连接的方式查询所有学生的信息和考试信息
 select * from score a,student b where a.stu_id=b.id;

-- 12.计算每个学生的总成绩
 select sum(grade) from score group by stu_id;

-- 13.计算每个考试科目的平均成绩
 select avg(grade) from score group by c_name;

-- 14.查询计算机成绩低于95的学生信息
 select * from score a,student b where a.stu_id=b.id and a.c_name='计算机' and a.grade<95;

-- 15.查询同时参加计算机和英语考试的学生的信息
select * from score a,student b where a.stu_id=b.id and a.c_name=(select c_name from score where  a.c_name='计算机' and a.c_name='英语');

-- 16.将计算机考试成绩按从高到低进行排序
select grade from score where c_name='计算机' order by grade desc;

-- 17.从student表和score表中查询出学生的学号，然后合并查询结果
 select a.stu_id from score a,student b where a.stu_id=b.id;

-- 18.查询姓张或者姓王的同学的姓名、院系和考试科目及成绩
select b.name,b.department,a.grade,a.c_name from score a join student b on a.stu_id=b.id where b.name like '张%' or b.name like '王%' ;

-- 19.查询都是湖南的学生的姓名、年龄、院系和考试科目及成绩
select b.name,b.birth,b.department,a.c_name,a.grade from score a join student b on a.stu_id=b.id where address like '%湖南%';
