//1 声明函数
function fn(num) {
    // 2定义一个空数组
    var arr = [];
    // 3声明变量
    var i = 2;
    // 4用while循环
    while (i <= num) {
        // 5判断 num % i是否等于0 是的话就把值赋值给空数组 并除以 i 如果不等于 0 i就自增一次
        if (num % i == 0) {
            arr.push(i);
            num /= i;
        } else {
            i++;
        }
    }
    // 6获取返回值
    return arr;
}
console.log(fn(25))