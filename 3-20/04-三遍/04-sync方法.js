// 引包
const fs = require('fs');
const path = require('path');


// // 异步(asynchronize)
// console.log('1231');
// fs.readFile(path.join(__dirname, "./file/result.txt"), 'utf-8', (error, data) => {
//     if (error) {
//         console.log(error);
//     }
//     console.log(data);
// })
// fs.readFile(path.join(__dirname, "./file/成绩.txt"), 'utf-8', (error, data) => {
//     if (error) {
//         console.log(error);
//     }
//     console.log(data);
// })
// console.log('1321');

// 同步
console.log("1");
var result1 = fs.readFileSync(path.join(__dirname, './file/result.txt'), 'utf-8');
console.log(result1);
console.log(2);
var result2 = fs.readFileSync(path.join(__dirname, './file/成绩.txt'), 'utf-8');
console.log('3');
console.log(result2);