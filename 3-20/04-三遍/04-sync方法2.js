// 引包
const fs = require('fs');
const path = require('path');

// // 异步
// console.log(1);
// setTimeout(() => {
//     console.log(2)
// }, 100);
// console.log(3);
// fs.readFile(path.join(__dirname, './file/result.txt'), 'utf-8', (error, data) => {
//     if (error) {
//         console.log(error);
//     }
//     console.log(data);
// })
// console.log('4');

// 同步
console.log('l');
setTimeout(() => {
    console.log(2)
}, 100);
var result = fs.readFileSync(path.join(__dirname, './file/result.txt'), 'utf-8');
console.log(result);
console.log(3);
var result1 = fs.readFileSync(path.join(__dirname, './file/成绩.txt'), 'utf-8');
console.log(result1);
console.log('4');