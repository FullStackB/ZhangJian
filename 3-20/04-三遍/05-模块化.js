//1 require
// console.log(require);

// 2 exports对象
console.log(exports);
console.log(module.exports);
console.log(module.exports == exports);

// 3 module对象
console.log(module);