let a = 10;
function fn() {
    console.log(888);
}
let arr = [12, 3, 4, 5];
let f1 = function () {
    console.log(111);
}

module.exports = {
    a: a,
    fn: fn,
    arr: arr
}

// 全局变量全局函数 这是明令禁止的
console.log(global);
global.a = 10;
global.show = function () {
    console.log('comebaby')
}