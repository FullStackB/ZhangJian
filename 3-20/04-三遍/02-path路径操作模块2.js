// node自带path
const path = require('path');

// path.sep 返回当前系统的片段符
console.log(path.sep);

let str = "D:/code/nodejs/day2/02-path路径操作模块.js";
// console.log(__dirname);
console.log(path.dirname(str));

// path.extname  extname扩展名后缀
console.log(path.extname(str));

// 拼接路径 path.join
console.log(__dirname + './file/成绩.txt');

console.log(path.join(__dirname, './file/成绩.txt'));

console.log(path.join('D:', '/code', '/nodejs', '/day1', '../day2/01-整理成绩.js'));