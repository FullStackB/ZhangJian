// 前置至少filter
// let arr = [13, 0, 123, 56];
// var result = arr.filter(function (item, index)) {
//    //  console.log(item);
//      return item >= 13;
// }
// var result = arr.filter((item) => { return item > 13 })
// console.log(result);


// 引入fs模块
const fs = require('fs');

// 使用fs模块的readFIle读取文件
fs.readFile(__dirname + '/file/成绩.txt', 'utf-8', (error, data) => {
    if (error) {
        console.log(error);
    }

    // 把取出来的字符串数据变成字符串数组(split)
    // 遍历数组
    var scoreArr = data.split(' ').filter((item) => { return item.length > 0 });
    console.log(scoreArr);
    // 把每一项的=换成：
    var newArr = scoreArr.map((item) => { return item.replace('=', ':') })
    console.log(newArr);
    // 把数组转换成字符串 join方法
    // 把处理完的数据写入新文件中
    fs.writeFile(__dirname + '/file/result.txt', newArr.join('\n'), 'utf-8', (error) => {
        if (error) {
            console.log('文件写入失败');
            return false;
        }
        console.log('compelte');
    })
})