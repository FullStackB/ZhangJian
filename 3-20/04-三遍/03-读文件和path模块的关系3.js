// 引包
const fs = require('fs');
const path = require('path');

console.log(path.join(__dirname, './file/result.txt'));

// 只要涉及到路径片段的拼接，一定要使用 path.join()方法
fs.readFile(path.join(__dirname, './file/result.txt'), 'utf-8', (err, dataStr) => {
    if (err) return console.log(err.message);
    console.log(dataStr);
})
