// 引包
const fs = require('fs');
const path = require('path');
// 异步
console.log('l');
fs.readFile(path.join(__dirname, "./file/result.txt"), 'utf-8', (error, data) => {
    if (error) {
        console.log(error);
    }
    console.log(data);
})
console.log(1);

// 同步
console.log(2);
var result1 = fs.readFileSync(path.join(__dirname, "./file/result.txt"), 'utf-8');
console.log('2')