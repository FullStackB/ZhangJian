// 07-公约数和公倍数.js
// 公倍数
function gb(a, b) {
    // 获得大的数max和小的数min
    let max = a > b ? a : b;
    let min = a <= b ? a : b
    // 遍历max到max和min乘积之间的整数， 如果能整除他们两就是最大公约数
    for (let i = max; i <= max * min; i++) {
        if (i % max == 0 && i % min == 0) {
            return i;
        }
    }

}

// console.log(gb(6, 9))

// 公约数
function gy(a, b) {
    let max = a > b ? a : b;
    let min = a <= b ? a : b
// 遍历min到0之间的数能被max和min整除就是公约数
    for (let j = min; j >= 0; j--) {
        if (max % j == 0 && min % j == 0) {
            return j;
        }
    }
}

// console.log(gy(1, 9));