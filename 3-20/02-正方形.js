// 、使用javascript脚本在页面上输出正方形，要求如下。
// （1）使用prompt()方法输入正方形的行数.
// （2）无论输入的正方形行数是否大于10，输出的正方形最多为10行。
// 提示:
// 》首先声明一个变量num来保存prompt()方法获取输入的行数
// 》使用if判断num是否大于10，如果大于十，就赋值num=10,
// 》可以使用双重循环显示正方形，外层显示行数，内层显示每行显示的“#个数”


// 输入num值
var num = prompt('请输入正方形的行数');

// 声明函数打印
function fan(num) {
    // num小于等于10行
    if (num >= 10) {
        return num = 10
    }

    for (let i = 0; i < num; i++) {//循环列数
        var fun = '';// 声明空字符串
        for (let j = 0; j < num; j++) {//行数
            fun += '*';//字符串相加
        }
        console.log(fun);//打印

    }
}
// 调用方法
fan(num);
