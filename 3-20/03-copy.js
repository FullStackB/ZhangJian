// 3、使用node.js拷贝文件   使用sync方法 同步进程
// 3.1、首先对文件‘03-copy.txt’写入内容“我准备拷贝文件”；
// 3.2、其次对文件‘03-copy.txt’追加内容“我继续追加信息到文件中”；
// 3.3、获取文件‘03-copy.txt’大小 单位是字节；
// 3.4、获取文件‘03-copy.txt’的创建时间
// 3.5、判断‘03-copy.txt’是否是一个文件
// 3.6、判断‘03-copy.txt’是不是目录
// 3.7、复制文件“03-copy.txt”为一个新的文件“03-copyFile.txt”
// 3.7、请在03-copy.js中完成

// 引包
const fs = require('fs');
const path = require('path');

// 写入
let result = fs.writeFileSync(path.join(__dirname, './file/03-copy.txt'), '我准备拷贝文件', 'utf-8');
console.log(result);
// 追加
let result1 = fs.appendFileSync(path.join(__dirname, './file/03-copy.txt'), '我继续追加信息到文件中', 'utf-8')
console.log(result1);
// 获取文件信息
let result2 = fs.statSync(path.join(__dirname, './file/03-copy.txt'));
console.log(result2.size);//大小 单位是字节
console.log(result2.birthtime);//创建时间
console.log(result2.isFile());//是否是一个文件
console.log(result2.isDirectory())//是不是目录
// 复制文件
let result3 = fs.copyFileSync(path.join(__dirname, "./file/03-copy.txt"), path.join(__dirname, "./file/03-copyFile.txt"), "utf8")
console.log(result3);