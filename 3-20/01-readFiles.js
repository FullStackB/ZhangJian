// 1、使用node.js对文件‘01-read.txt’中的内容“我是传智专修学院，全栈应用方向专业的一名大大一学生，我非常自豪”进行读取，并且在控制台输出，请在01-readFile.js中完成



// 引包
const fs = require('fs');
const path = require('path');


// 读取
fs.readFile(path.join(__dirname, './file/01-read.txt'), 'utf-8', (err, data) => {
    if (err) return console.log(err.message);//出错就返回错误
    console.log(data);//正常就打印读取的数据
})