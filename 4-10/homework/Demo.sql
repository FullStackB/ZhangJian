-- 建表，名字分别是Student,COurse,Score,Teacher
create table Student(Sno Char(3) not null, Sname Char(8) not null,Ssex Char(2) not null,Sbirthdy datetime not null, Class Char(5) not null);
create table COurse(Cno Char(5) not null, Cname varchar(10) not null,Tno Char(3) not null);
create table Teacher(Tno Char(3) not null, Tname Char(4) not null,Tsex Char(2) not null,Tbirthday datetime,Prof Char(6),Depart varchar(10) not null);
create table Score(Sno Char(3) not null, Cno Char(5) not null, Degree Decimal(4,1));

-- 输入数据
--表student数据
insert into student values("108",'曾华','男','1977-09-01','95033'),("105",'匡明','男','1975-10-02','95031'),("107",'王丽','女','1976-01-23','95033'),("101",'李军','男','1976-02-20','95033'),("109",'王芳','女','1975-02-10','95031'),("103",'陆君','男','1974-06-03','95031');
--teacher插入数据
insert into teacher values('804','李诚','男','1958-12-02','副教授','计算机系'),('856','张旭','男','1969-03-12','讲师','计电子工程系'),('825','王萍','女','1972-05-05','助教','计算机系'),('831','刘冰','女','1977-08-14','助教','电子工程系');
--表course插入数据
insert into course values('3-105','计算机导论','825'),('3-245','操作系统','804'),('6-166','数字电路','856'),('9-888','高等数学','831');
--score分数表
insert into score values('103','3-245',86),('105','3-245',68),('109','3-245',68),('103','3-105',92),('105','3-105',88),('109','3-105',76),('101','3-105',64),('107','3-105',91),('108','3-105',78),('101','6-166',85),('107','6-166',79),('108','6-166',81);