// 引入数据库连接
const connection = require('../data');

// 首页展示
module.exports.index = (req, res) => {
    res.render('index.html');
}

// 添加用户
module.exports.createUser = (req, res) => {
    // 使用数据库连接的query方法添加用户
    connection.query('insert into users(username, password)values("' + req.body.username + '","' + req.body.password + '")', (error, result) => {
        if (error) {
            console.log(error);
        } else {
            if (result) {
                res.json({
                    code: '10000',
                    msg: "添加用户成功"
                })
            }
        }
    })
}

module.exports.showUser = (req, res) => {
    connection.query('select * from users', (error, result) => {
        if (error) {
            console.log(error);
        } else {
            console.log(result);
            res.json(result);
        }
    })
}

module.exports.delUser = (req, res) => {
    connection.query('delete from users where id =' + req.query.id, (error, result) => {
        if (error) {
            console.log(error);
        } else {
            res.json({
                code: '10001',
                msg: '删除成功'
            })
        }
    })
}