const express = require('express');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));

const route = require('./route');
app.use(route);
app.engine('html', require('express-art-template'));
app.set('views', 'views');

app.use(express.static('./public'));
app.listen(80, () => {
    console.log("请访问: http://localhost");
})