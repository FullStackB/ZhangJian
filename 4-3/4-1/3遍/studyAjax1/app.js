// 引入框架
const express = require('express');

// 创建服务器
const app = express();

// 配置接受post请求的body-parser
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
// 配置静态目录
app.use('/public', express.static('public'));
// 请求/的时候返回get.html
app.get('/', (req, res) => {
    res.sendFile('./views/post.html', { root: __dirname });
})
// 接受表单传来的数据
app.post('/user', (req, res) => {
    res.send(req.body);
})
app.get('/user', (req, res) => {
    res.send(req.query.username + '来吧');
})
app.listen(80, () => {
    console.log("http://127.0.0.1");
})