const express = require('express');
const router = express.Router();
const controller = require('../controller');
router.get('/', controller.index);
router.post('/createUser', controller.createUser);
router.get('/show', controller.showUser);
router.get('/deluser', controller.delUser);
module.exports = router;