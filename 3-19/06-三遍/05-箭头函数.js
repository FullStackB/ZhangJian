var fz = function (x, y) {
    console.log(x + y);
}
fz(1, 2);

let fn = (x, y) => {
    console.log(x + y);
}
fn(1, 2);

// 箭头函数的变体
var fa = name => console.log(name);
fa('name');

var fq = () => 1;
var result = fq();
console.log(result);

// this指向问题

var num = 10;

for (let i = 0; i < 100; i++) {
    if (i == num) {
        setTimeout(() => {
            console.log(this);
        }, 100);
    }
}
