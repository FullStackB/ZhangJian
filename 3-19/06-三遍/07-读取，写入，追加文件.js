// 引包
const fs = require('fs');

// 读取
fs.readFile('./file/1.txt', 'utf8', (error, data) => {
    if (error) {
        console.log(error.message);
    }
    console.log(data);
})

// 网页的字符串
let str = `<html>
<head>
</head>
<body>
    rabbit dragon<br/>
    best match
</body>
</html>`
// 写入
fs.writeFile('./file/index.html', str, 'utf8', (err) => {
    if (err) {
        console.log(err);
    }
})
// 追加
fs.appendFile('./file/1.txt', '诶？诶？大咩daze', 'utf8', (error) => {
    if (error) {
        console.log(error);
    }
    console.log('compelte');
})