// 引包
const fs = require('fs');

// 读取
fs.readFile('./file/index2.html', 'utf8', (error, data) => {
    if (error) {
        console.log(error.message);
    }
    console.log(data);
})

let str = `<html>
<head>
</head>
<body>
</body>
</html>`
// 写入
fs.writeFile('./file/index.html', str, 'utf8', (err) => {
    if (err) {
        console.log(err);
    }
})

// 追加
fs.appendFile('./file/index.html', '<script></script>', 'utf8', (error) => {
    if(error) {
        console.log(error);
    }
    console.log('compelte');
})