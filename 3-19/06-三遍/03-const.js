// const a = 3;
// a = 4;
// console.log(a);//const声明的常量不能改变

// // const b;
// // b = 1;//const声明的常量必须立即赋值

// const c = 1;
// // const c = 1;//const不能重复声明常量

// // const的常量值真的不能变吗？
// var obj = {
//     name: 'zhangjian'
// }
// obj.age = 18;

const obj = {
    name: 'zj'
}
obj.age = 2000;

console.log(obj);