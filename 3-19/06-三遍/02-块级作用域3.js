var age = 10;
function fn() {
    console.log(age);
    if (false) {
        var age = '???';
    }
}
fn();//undefined

var level = 99;
function lvup() {
    console.log(level);
    if (false) {
        let level = 100;
    }
}
lvup();

var hazzrdlv = 7.0;
function match() {
    var hazzrdlv;
    console.log(hazzrdlv);
    if (false) {
        hazzrdlv = 'crosszbuild betheone';
    }
}
// match();

// 用来计数的变量泄露为全局变量
for (var k = 0; k < 10; k++) {

}
// var k = 100;
console.log(k);

for (let i = 0; i < 10; i++) {

}
console.log(i);