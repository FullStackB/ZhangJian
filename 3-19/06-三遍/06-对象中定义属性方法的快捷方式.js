let name = 'zs';
let age = 119;
let sex = 'boy';

var obj = {
    name: name,
    age: age,
    sex: sex,
    sayHi: function () {
        console.log(123);
    }
}

let off = {
    name,
    age,
    sex,
    sayHi() {
        console.log(321);
    }
}

console.log(obj);
obj.sayHi();
console.log(off);
off.sayHi();