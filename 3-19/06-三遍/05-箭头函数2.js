let fb = (x, y) => {
    console.log(x + y);
}

fb(1, 2);

// 变体写法
var fa = name => console.log(name);
fa('bujidao');

var Person = (name, age) => {
    this.name = name;
    this.age = age;
}
var per = new Person('wo', 18);//Person is not a constructor
console.log(per);
// 箭头函数中的this 就是没有this
// 箭头函数不能作为构造函数使用