var age = 10;
function fn() {
    console.log(age);
    if (false) {
        var age = 'hello';
    }
}
fn();//这样就只能undefined

var height = 10;
function fx() {
    console.log(height);
    if (false) {
        let height = 'hi';//使用let就没有这些问题
    }
}
fx();

var sex = 1;
function fz() {
    console.log(sex);
    if (false) {
        sex = 'boy';//什么都不用也没问题
    }
}
fz();

// 2.用来计数的循环变量泄露为全局变量
for (var i = 0; i < 10; i++) {

}
var i = 100;
console.log(i);
for (let j = 0; j < 10; j++) {

}
console.log(j);// j is not defined