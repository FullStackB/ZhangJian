var fa = (a, b) => {
    console.log(a + b);
}

fa(1, 2);

// 只有一个参数的变体写法
var fb = name => console.log(name);
fb('wojiushiwo');

var Person = function (name, age) {
    this.name = name;
    this.age = age;
}
var man = new Person('heh', 1);
console.log(man);



var human = (name, age) => {
    this.name = name;
    this.age = age;
}// human is not a constructor
var humanben = new human('heh', 1);
console.log(humanben);