// ES6 允许在对象之中直接写变量 这时属性名为变量名，属性值为变量的值

let name = 'wo';
let age = 18;
let sex = 1;

var obj = {
    name,
    age,
    sex
}

console.log(obj);