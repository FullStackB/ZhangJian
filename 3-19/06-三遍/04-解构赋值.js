const person = {
    name: 'gr',
    age: 19,
    sex: 'man',
    height: '180cm',
    weight: '100mol'
}

// var { name: a, age: b, sex: c, height: d, weight: f } = person;
// console.log(a, b, c, d, f);

// var { name: a, age: b, sex: c, height: d, weight: e } = person;
var { name, age, sex, height, weight } = person;
console.log(name, age, sex, height, weight); 