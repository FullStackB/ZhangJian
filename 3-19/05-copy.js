// 首先对文件‘05-copy.txt’写入内容“我准备拷贝文件”；
// 5.2、其次对文件‘05-copy.txt’追加内容“我继续追加信息到文件中”；
// 5.3、获取文件‘05-copy.txt’大小 单位是字节；
// 5.4、获取文件‘05-copy.txt’的创建时间
// 5.5、判断‘05-copy.txt’是否是一个文件
// 5.6、判断‘05-copy.txt’是不是目录
// 5.7、复制文件“05-copy.txt”为一个新的文件“05-copyFile.txt”
// 5.7、请在05-copy.js中完成

// 引包
const fs = require('fs');
//追加
fs.appendFile('./05-copy.txt', '我继续追加信息到文件中', 'utf8', (error) => {
    if (error) {
        console.log(error);
    }
})
// 写入
fs.writeFile('./05-copy.txt', '我准备拷贝文件', 'utf8', (err) => {
    if (err) {
        console.log(err);
    }
})
// 查看
fs.stat(__dirname + '/05-copy.txt', (error, stats) => {
    if (error) {
        console.log(error);
    }

    // 文件的大小  单位是字节 byte
    console.log(stats.size);
    // 文件的创建时间
    console.log(stats.birthtime.toString());
    // 判断该文件是不是文件
    console.log(stats.isFile());
    // 判断该文件是不是文件夹
    console.log(stats.isDirectory());
})

fs.copyFile(__dirname + '/05-copy.txt', __dirname + '/05-copyFile.txt', (err) => {
    console.log(err);
})
fs.copyFile(__dirname + '/05-copy.txt', __dirname + '/05-copyFile.txt', (err) => {
    console.log(err);
})