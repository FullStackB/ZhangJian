// 3、使用node.js完成如下功能
// 3.1、首先对文件‘03-write.txt’写入内容“我是传智专修学院的大学生，我要好好学习”；
// 3.2、其次对文件‘03-write.txt’追加内容“我是最棒的”；
// 3.3、最后读取“03-write.txt”整个文件内容显示到控制台；
// 3.4、请在03-appendFile.js中完成

const fs = require('fs');  // 引包

// 写入内容
fs.writeFile('./03-write.txt', '我是传智专修学院的大学生，我要好好学习', 'utf8', (err) => {
    if (err) {
        console.log(err);
    }
})

// 追加
fs.appendFile('./03-write.txt', '我是最棒的', 'utf8', (error) => {
    if (error) {
        console.log(error);
    }
})

// 读取
fs.readFile('./03-write.txt', 'utf8', (error, data) => {
    if (error) {
        console.log(error.message);//如果错误输出错误信息
    }
    console.log(data);
})