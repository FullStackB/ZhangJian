// 2、使用node.js对文件‘02-write.txt’写入内容“我是传智专修学院的大学生，我要好好学习”，并且读取刚才写入的内容显示到控制台，

const fs = require('fs')//引包

let str = '我是传智专修学院的大学生，我要好好学习';//要写入的字符串

// 写入
fs.writeFile('./02-write.txt', str, 'utf8', (err) => {
    if (err) {
        console.log(err);//如果出错就打印错误
    }
})

// 读取
fs.readFile('./02-write.txt', 'utf8', (error, data) => {
    if (error) {
        console.log(error.message);//如果出错就打印错误
    }
    console.log(data);//打印读取的内容
})