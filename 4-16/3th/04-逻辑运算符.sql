-- and、or、not 逻辑运算符

-- 找出 having_data表中 年龄在18到30岁的人
-- mysql中 条件的第一个值 一定是小于后面的值的
select * from having_data  where age>=18 and age<=30;
select * from having_data  where age<=30 and age>=18;(应该有版本之差 如果语句可以执行 那就用 但是还是建议 小的条件在前 大的条件在后)
-- or: 逻辑或 ||
-- 找出 女性 或 年龄小于18的人
select * from having_data where sex="男" or age<=18;
-- not: 逻辑非 !

select * from having_data where not sex="男";