-- in运算符

-- 基本语法： in (结果1,结果2,结果3…)，只要当前条件在结果集中出现过，那么就成立

-- 显示 id为1  3  5 6  8的用户的信息
select * from having_data where id in(1,3,5,6,8);


-- is运算符
-- Is是专门用来判断字段是否为NULL的运算符
-- 基本语法：is null / is not null

-- 找出  having_data中 age不为null的数据
select * from having_data where age is not null;

create table is_data(
  int_1 int ,
  int_2 int
);

insert into is_data values
(null, 10),
(100,200);
