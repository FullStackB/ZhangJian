-- 创建数据表
create table having-data(   
        id int primary key auto-increment,
        name varchar(6) not null,
        sex enum('boy','girl') default '保密',
        age tinyint not null default 18
    );


--  插入数据
insert into having_data(name, sex, age) values 
('1','2',3),
('4'.'5',6),
('7','8',9);
 
select group_concat(name),count(8) from having_data group by sex having count(*)>=2;