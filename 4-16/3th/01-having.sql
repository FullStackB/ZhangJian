-- 创建数据库
create database day06;

-- 创建数据表
create table having_data(
  id int primary key auto_increment,
  name varchar(6) not null,
  sex enum('男','女','保密') default'保密',
  age tinyint not null default 18
);

-- 插入数据
insert into having_data(name,sex,age) values
('1','2',3),
('2','3',4),
('3','4',5),
('5','2',6),
('1','2',7);

-- 先找到having_data 的所有数据 按照 性别进行分组 显示  个数  
select count(*) from having_data group by sex;

-- 想让 数量大于=6的那一组 显示名字
select group_concat(name),count(*) from having_data group by sex;

-- select group_concat(name),count(*) from having_data group by sex;
-- where 不能使用聚合函数
-- 
--  select group_concat(name),count(*) from having_data where count(*) >=6 group by sex;  错误的

select group_concat(name),count(*) from having_data  group by sex having count(*)>=6; 
