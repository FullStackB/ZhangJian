-- 创建一张表
create table union_data(
  stu_id varchar(6) primary key,
  stu_name varchar(5) not null,
  stu_age tinyint not null default 18,
  stu_height varchar(4) not null,
  stu_set enum('男','女','保密') default '保密'
);

-- 添加数据
insert into union_data values
('stu001', '小红',default, '156','女'),
('stu002', '小绿',21, '156','男'),
('stu003', '小黑',20, '158','女'),
('stu004', '小白',17, '168','男'),
('stu005', '小婷',16, '175','女'),
('stu006', '小雅',25, '170','男'),
('stu007', '小石头',26, '180','女'),
('stu008', '小蒋',29, '170','男'),
('stu009', '小贾',20, '174','女');


-- 显示性别为男性的数据
select * from union_data where stu_set ='男';
-- 显示性别为女性的数据
select * from union_data where stu_set="女";

-- 使用联合查询
-- 显示性别为男性的数据
(select * from union_data where stu_set ='男')
union
-- 显示性别为女性的数据
(select * from union_data where stu_set="女");

-- 使用联合查询 显示男性数据升序 显示女性数据降序
(select * from union_data where stu_set ='男' order by stu_height asc limit 100)
union
-- 显示性别为女性的数据
(select * from union_data where stu_set="女" order by stu_height desc limit 100);