-- >、>=、<、<=、=、<>
-- 通常是用来在条件中进行限定结果
-- =：在mysql中，没有对应的 ==比较符号，就是使用=来进行相等判断
-- <=>：相等比较

-- 找出 having_data中年龄大于20的人
-- select * from having_data  where age >=20;

-- -- 找出 having_data中 性别为女的人
-- select * from having_data  where sex <=> "女";


-- select * from having_data  where sex <> "保密";


-- mysql中 没有规定 select 后面必须跟 表名

select '1'<=>1, 0.002 <=>0,0.002<>0;