-- 1.创建student和score表
create table student(
    id primary key auto_increment comment 'id',
    name varchar not null comment '姓名',
    department varchar not null comment '派系',
    age int default 0 comment '年龄',
    homeplace varchar not null comment '产地'
)
create table score (
    name varchar comment '姓名',
    c_name varchar comment '科目',
    grade float(5,2) comment '成绩'

)

-- 2.为student表和score表增加记录
insert into student values 
(1,'张王','大麻种植与加工,销售系',18,'缅甸');
insert into score values 
('张王','大麻种植与加工,销售系',13);
-- 3.查询student表的所有记录
select * from student;
-- 4.查询student表的第2条到4条记录
select * from student limit 1,4;
-- 5.从student表查询所有学生的学号（id）、姓名（name）和院系（department）的信息
select id,name,department from student;
-- 6.从student表中查询计算机系和英语系的学生的信息
select * from student where department in ('计算机系','英语系');
-- 7.从student表中查询年龄18~35岁的学生信息
select * from student where age between 18 and 25;
-- 8.查询每个院系有多少人
select count(*) from student group by department;
-- 9.查询每个科目的最高分
select c_name,grade from score group by c_name where grade = (select max(grade) from student group by c_name);
-- 10.查询李四的考试科目（c_name）和考试成绩（grade）
select c_name,grade from score where name like '李四'；
-- 11.所有学生的信息和考试信息
select * from student;
select * from score;
-- 12.计算每个学生的总成绩
select count(score) from score group by name;
-- 13.计算每个考试科目的平均成绩
select department,avg(score) from score group by department;
-- 14.查询计算机成绩低于95的学生信息
select * from student where name = (select name from score where score < 95);
-- 15.查询同时参加计算机和英语考试的学生的信息
select *  from student where name = (select name from score where department = '计算机' and name in (select name from student where department = '英语'))
-- 16.将计算机考试成绩按从高到低进行排序
select score from score where department  = '计算机' order by score desc;
-- 17.查询姓张或者姓王的同学的姓名、院系和考试科目及成绩
select s.name,s.department,sc.department,sc.score  from student s,score sc where name like '张%' or name like '王%' and s.name = sc.name;
-- 18.查询都是湖南的学生的姓名、年龄、院系和考试科目及成绩
select s.name,s.age,s.department,sc.department,sc.score from student s,score sc where homeplace = '湖南' and s.name = sc.name;