let role = require("./tree_data.js");
// console.log(role);

// 请自己想办法把这个role中的权限id存到一个空数组中
// leaf 叶子
// role代表找那个对象的叶子
function getLeafId(role,keyArr) {
  // 只有当该节点没有子元素的时候 我才会把id存到该数组
  if(!role.children) {
    return keyArr.push(role.id);
  }

  // 如果role.children有孩子
  role.children.forEach(item => getLeafId(item,keyArr));
}

let keys = [];
getLeafId(role, keys);
console.log(keys);
