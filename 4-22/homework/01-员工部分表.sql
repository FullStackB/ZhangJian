-- 建表 部分表dept
CREATE TABLE dept  (
DEPTNO FLOAT(2) PRIMARY KEY, -- 部门号
DNAME VARCHAR(14) ,  -- 部门名称
LOC VARCHAR(13) -- 部门地址
) ;  


-- 部门表dept插入数据
INSERT INTO dept VALUES
(10,'ACCOUNTING','NEW YORK'),
(20,'RESEARCH','DALLAS'),
(30,'SALES','CHICAGO'),
(40,'OPERATIONS','BOSTON');

-- 建表 员工表EMP
CREATE TABLE emp  
(EMPNO float(4)  PRIMARY KEY,  -- 员工编号
ENAME VARCHAR(10),  -- 员工姓名
JOB VARCHAR(9),  -- 员工职位
MGR float(4),  -- 员工上级工号
HIREDATE DATE,  -- 生日
SAL float(7,2),  -- 薪水
COMM float(7,2),  -- 年终奖
DEPTNO float(2) REFERENCES dept -- 部门号
); 

-- 员工表插入数据
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7369, 'SMITH', 'CLERK', 7902, '1980-12-17', 800, null, 20);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7499, 'ALLEN', 'SALESMAN', 7698, '1981-02-20', 1600, 300, 30);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7521, 'WARD', 'SALESMAN', 7698, '1981-02-22', 1250, 500, 30);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7566, 'JONES', 'MANAGER', 7839, '1981-04-02', 2975, null, 20);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7654, 'MARTIN', 'SALESMAN', 7698, '1981-09-28', 1250, 1400, 30);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7698, 'BLAKE', 'MANAGER', 7839, '1981-05-01', 2850, null, 30);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7782, 'CLARK', 'MANAGER', 7839, '1981-06-09', 2450, null, 10);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7788, 'SCOTT', 'ANALYST', 7566, '1987-04-19', 3000, null, 20);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7839, 'KING', 'PRESIDENT', null, '1981-11-17', 5000, null, 10);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7844, 'TURNER', 'SALESMAN', 7698, '1981-09-08', 1500, 0, 30);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7876, 'ADAMS', 'CLERK', 7788, '1987-05-23', 1100, null, 20);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7900, 'JAMES', 'CLERK', 7698, '1981-12-03', 950, null, 30);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7902, 'FORD', 'ANALYST', 7566,'1981-12-02', 3000, null, 20);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7934, 'MILLER', 'CLERK', 7782, '1982-01-23', 1300, null, 10);


-- ##### 1) 查询没有上级的员工全部信息（mgr是空的）
select * from emp where mgr is null;

-- ##### 2) 列出30号部门所有员工的姓名、薪资
select ename,sal from emp where deptno = 30;

-- ##### 3)  查询姓名包含 'A'的员工姓名、部门名称 
select b.dname,a.ename from dept b join emp a on a.deptno = b.deptno where a.ename like '%A%';

-- ##### 4) 查询员工“TURNER”的员工编号和薪资
select empno,sal from emp where ename = 'turner';

-- ##### 5) -- 查询薪资最高的员工编号、姓名、薪资
select empno,ename,max(sal) from emp;

-- ##### 6) -- 查询10号部门的平均薪资、最高薪资、最低薪资
select avg(sal),max(sal),min(sal) from emp where deptno = 10;
 
-- #### 子查询

-- ##### 1) 列出薪金比员工“TURNER”多的所有员工姓名（ename）、员工薪资（sal）
select ename,sal from emp where sal > (select sal from emp where ename = 'TURNER');

-- ##### 2) 列出薪金高于公司平均薪金的所有员工姓名、薪金。
select ename,sal from emp where sal > (select avg(sal) from emp);


-- ##### 3) 列出与“SCOTT”从事相同工作的所有员工姓名、工作名称(不展示Scott的姓名、工作)
select ename,job from emp where job = (select job from emp where ename = 'SCOTT') and ename <> 'SCOTT';

-- ##### 4) 列出薪金高于30部门最高薪金的其他部门员工姓名、薪金、部门号。
select ename,sal,deptno from emp where sal > (select max(sal) from emp where deptno = 30);

-- ##### 5) -- 查询薪资最高的员工编号、姓名、薪资
select empno,ename,sal from emp where sal = (select max(sal) from emp);

-- ##### 6) 列出薪金高于本部门平均薪金的所有员工姓名、薪资、部门号、部门平均薪资。
select emp.ename,emp.sal,emp.deptno,t.avgsal from emp ,(select deptno,avg(sal) avgsal from emp group by deptno) t where emp.deptno = t.deptno and emp.sal > t.avgsal;