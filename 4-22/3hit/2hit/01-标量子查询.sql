(null, '虚竹', 26, 4);

-- 标量子查询：子查询得到结果是一个数据（一行一列）

-- 基本语法：select * from 数据源 where 条件判断 =/<> (select 字段名 from 数据源 where 条件判断); 

-- 1. 知道学生的姓名可以求出学生的班级id
select class_id from stu_info where info_name = "吴大龙";

-- 根据班级id求出班级的名称
select class_name  from stu_class where class_id = (select class_id from stu_info);