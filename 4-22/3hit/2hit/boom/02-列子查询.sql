-- 列子查询： 子查询得到的结果是一列数据（一列多行）

-- 基本语法： 主查询 where 条件 in （列子查询）

-- 火球已经有学生在班的班级名字

-- 1. 班级名字是通过班级的id得到的
-- 2.班级id是通过学生的班级id得到的
select * from stu_class where class_id in (select class_id from stu_info);