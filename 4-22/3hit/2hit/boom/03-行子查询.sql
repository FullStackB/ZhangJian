-- 行子查询
-- 概念： 子查询返回的结果是一行多列

-- 字段元素： 一个字段对应的值
-- 行元素: 就是多个字段， 多个字段合起来作为一个元素参与运算，把这种情况称之为行元素

-- 语法：主查询 where 条件 [构造一个行元素] = (行子查询);

--找出全校年龄最大的人的年龄
select max(info_age) from stu_info;
-- 通过年龄找到这个年龄最大的人的信息
select * from stu_info where info_age = (where max (info_age) from stu_info);