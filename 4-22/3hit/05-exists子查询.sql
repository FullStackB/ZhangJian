-- exists 子查询
-- 概念 ：查询返回的结果只有0或1,1表示成立2,0表示不成立

-- 语法：
-- 基本语法： where exists(查询语句);//exists就是根据查询得到的结果进行判断: 如果存在返回1不存在就返回0
 

-- 需求：求出，有学生在的所有班级的所有信息

-- 1.死的专业 你知道那个专业现在没有学生吗？
select * from stu_class where exists (select * from stu_info where stu_class.class_id = stu_info.class_id);