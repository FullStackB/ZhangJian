-- 创建数据库
create database day07_b;
-- 选择数据库
use day07_b;
-- 创建数据表-> 存储班级信息的
create table stu_class(
  class_id int primary key auto_increment,
  class_name varchar(20) not null
);

-- 创建学生信息表 -> 存储学生信息的
create table stu_info(
  info_id int primary key auto_increment,
  info_name varchar(10) not null,
  info_age tinyint unsigned,
  class_id int
);

set names gbk;

-- 向 班级信息表 插入数据
insert into stu_class values
(null, 'Java应用方向'),
(null, '全栈应用方向'),
(null, 'python应用方向'),
(null, '大数据应用方向');
-- 向 学生信息表  插入数据
insert into stu_info values
(null, '小鱼儿', 30, 1),
(null, '段誉', 18, 2),
(null, '木婉清', 16, 3),
(null, '虚竹', 23, 4),
(null, '乔峰', 32, 1),
(null, '黄眉僧', 50, 2),
(null, '扫地僧', 60, 3),
(null, '钟万仇', 44, 4),
(null, '云中鹤', 30, 1),
(null, '萧远山', 56, 2),
(null, '萧炎', 24, 3),
(null, '西门庆', 26, 4);

-- 标量子查询：子查询得到结果是一个数据（一行一列）

-- 基本语法：select * from 数据源 where 条件判断 =/<> (select 字段名 from 数据源 where 条件判断); 

-- 1. 知道学生的姓名可以求出学生的班级id
select class_id from stu_info where info_name = "吴大龙";

-- 根据班级id求出班级的名称
select class_name  from stu_class where class_id = (select class_id from stu_info);