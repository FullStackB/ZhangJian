-- 表子查询
-- 概念：子查询返回的结果是多行多列，表子查询与行子查询非常的相似， 只是行子查询需要产生行元素，而表子查询没有。

-- 基本语法
-- select 字段表 from (表子查询) as 别 名 [where] [group by] [having] [order by] [limit];


-- 1先把全校的人进行年龄的排版
select * from stu_info order by info_age desc;
-- 2.把上面的人的信息进行按照班级进行分组
select * from (select * from stu_info order by info_age desc) as temp  group by class_id;