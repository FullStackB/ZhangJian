// 读取package-lock.json文件,并将内容打印在控制台上，使用readFileSync读取，要求：
// 2.1-将读取到的数据转换为对象打印在控制台
// 2.2-将对象再次转换为字符串再次打印在控制台
// 2.3-通过【wirteFileSync】写入一个新的文件 newJson.txt


// 引包
const fs = require('fs');
const path = require('path');
// 读取
fs.readFileSync(path.join(__dirname, './file/package-lock.json'), 'utf-8', (error, data) => {
    if (error) {
        console.log(error);//出错就打印错误
    }
    var dataStr = data;//接收数据
    var dataObj = JSON.parse(dataStr); // 转换为对象
    console.log(dataObj); // 输出
    var newStr = JSON.stringify(dataObj) //对象再次转换为字符串
    console.log(newStr);//输出
})

// 写入一个新的文件 newJson.txt
fs.writeFileSync(path.join(__dirname, './file/newJson.txt'), '',  (err) => {
    console.log('compelte');
})