const add = require('./lib/add');
const minus = require('./lib/minus');
const division = require('./lib/division');
const multiply = require('./lib/multiply');
const remainder = require('./lib/remainder')

console.log(add);

module.exports = {
  add,
  minus,
  division,
  multiply,
  remainder
}