var jsonArr = '[1,2,3,4,5]';
var jsonobj = '{"name": "delete", "age": 18}';

// JSON.parse把字符串变成对象或数组
var arr = JSON.parse(jsonArr);
console.log(JSON.parse(jsonArr));
console.log(arr instanceof Object)
console.log(JSON.parse(jsonobj));

// JSON.stringify把对象或数组变成json字符串
var arr = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'];
var obj = {
    name: 'j',
    age: 'k'
}
console.log(JSON.stringify(arr));
console.log(JSON.stringify(obj));