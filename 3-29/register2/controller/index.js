const mysql = require('mysql');

const connection = mysql.createConnection({
  host: '127.0.0.1',
  port: "3306",
  user: "root",
  password: "root",
  database: "user"

})

module.exports.index = (req, res) => {
  res.render('index.html');
}

module.exports.adduser = (req, res) => {

  connection.query('INSERT INTO users(username,password) VALUES ("' + req.body.username + '","' + req.body.password + '")', (error, result) => {
    if (error) {
      console.log(error);
    } else {
      // 当我插入数据成功之后  我要跳转到/show这个页面
      res.redirect('/show');
    }
  })
}

// 查询数据
module.exports.show = (req, res) => {
  connection.query("select * from users", (error, result) => {
    if (error) {
      console.log(error);
    } else {
      console.log(result);
      res.render('show.html', { list: result });
    }
  })
}

// 删除用户
// 1.在show.html中 写js代码 当点击删除的时候把id传给后台 去请求对应的接扣
module.exports.deluser = (req, res) => {
  //  DELETE FROM 表名 where id =值
  connection.query("delete from users where id =" + req.query.id, (error, result) => {
    if (error) {
      console.log(error);
    } else {
      console.log(result);
    }
  })

  res.redirect('/show');
}