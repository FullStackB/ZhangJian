const express = require('express');
const app = express();

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }))

app.use('/public', express.static('public'));
app.engine('html', require('express-art-template'));
app.set('views', 'views');
const router = require('./route');
app.use(router);

app.listen(80, () => {
    console.log("http://localhost:80");
})