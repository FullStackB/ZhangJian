const express = require('express');
const router = express.Router();

const controller = require('../controller');

router.get('/', controller.index);
router.post('/adduser', controller.adduser)
router.get('/show', controller.show);
router.get('/deluser', controller.deluser);

module.exports = router;