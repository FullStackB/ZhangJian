const mysql = require('mysql');

const connection = mysql.createConnection({
    host: '127.0.0.1',
    port: '3306',
    user: "root",
    password: "root",
    database: "user"
})

module.exports.index = (req, res) => {
    res.render('index.html');
}
module.exports.adduser = (req, res) => {
    connection.query('INSERT INTO users(username, password) VALUE("' + req.body.username + '","' + req.body.password + '")', (error, result) => {
        if (error) {
            console.log(error);
        } else {
            res.redirect('/show');
        }
    })
}

module.exports.show = (req, res) => {
    connection.query("select * from users", (error, result) => {
        if (error) {
            console.log(error);
        } else {
            console.log(result);
            res.render('show.html', { list: result });
        }
    })
}

module.exports.deluser = (req, res) => {
    connection.query("delete from users where id =" + req.query.id, (error, result) => {
        if (error) {
            console.log(error);
        } else {
            console.log(result);
        }
    })
    res.redirect('/show');
}