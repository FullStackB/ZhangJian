//导入http模块
const http = require('http');
// 使用http模块创建一个服务器
const server = http.createServer();
// 记录次数的变量
let con = 0;
// 通过服务器的绑定事件的方法on监听客户端的请求
server.on('request', (req, res) => {
    console.log(con++);
    res.end("please give me a pan");
})
// 启动并监听指定的端口
server.listen(3000, () => {
    console.log("come on: http://127.0.0.0");
})