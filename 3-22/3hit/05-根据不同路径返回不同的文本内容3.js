// 导入http模块
const http = require('http');
const fs = require('fs');
const path = require('path');
// 使用http模块创建服务器
const server = http.createServer();
// 监听请求
server.on('request', (req, res) => {
    res.writeHead(200, { "Content-Type": "text/html;charset=UTF-8" });
    console.log(req.url);
    if (req.url === '/' || req.url === "/index.html") {
        fs.readFile(path.join(__dirname, './index.html'), 'utf8', (error, data) => {
            if (error) {
                console.log(error);
            }
            res.end(data);
        })
    } else if (req.url === "/about.html") {
        fs.readFile(path.join(__dirname, './about.html'), 'utf8', (error, data) => {
            if (error) {
                console.log(error);
            }
            res.end(data);
        })
    }
})
server.listen(80, () => {
    console.log("wellcome http://127.0.0.5:80")
})