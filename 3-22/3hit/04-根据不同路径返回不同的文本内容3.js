// 导入 http模块
const http = require('http');
// 使用http模块创建服务器
const server = http.createServer();
// 监听请求
server.on('request', (req, res) => {
    res.writeHead(200, { "Content-Type": "text/html;charset=UTF-8" });
    if (req.url === '/' || req.url === "/index.html") {
        res.end(`<!DOCTYPE html>
        <html lang="en">
        
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Document</title>
        </head>
        
        <body>
            <div>这是谁</div>
        </body>
        
        </html>`);
    } else if (req.url === "/about.html") {
        res.end(`<!DOCTYPE html>
        <html lang="en">
        
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Document</title>
        </head>
        
        <body>
            <span>皮卡丘</span>
        </body>
        
        </html>`);
    }
})
// 指定端口
server.listen(80, () => {
    console.log("请访问http://127.0.0.1:80");
})