// 1导入模块
const http =  require('http');
// 2使用http模块创建一个服务器
const server = http.createServer();
// 3通过服务器的绑定事件的方法on箭头客服端的请求
server.on('request', (req, res) => {
    res.end('goldren wind')
})
// 4. 启动并监听指定的端口
server.listen(3000, () => {
    console.log("来吧：http://127.0.0.2:3000")
})