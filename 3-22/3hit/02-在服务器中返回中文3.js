// 导入http模块
const http = require('http');
// 使用http模块创建服务
const server = http.createServer();
// 监听请求事件
server.on('request', (req, res) => {
    res.writeHead(200, { "Content-Type": "text/plain;charset=UTF-8" });
    res.end('这是中文')
})
// 监听端口
server.lister(3001, () => {
    console.log("http://127.0.0.2:3001")
})