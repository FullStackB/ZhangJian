// 3、使用node.js完成根据不同url显示不同的内容，要求
// 3.1、开启服务，并且使用8899端口；
// 3.2、访问”http://127.0.0.1:8899/home.html”显示“首页”
// 3.3、访问”http://127.0.0.1:8899/study.html”显示“学习”；
// 3.4、访问”http://127.0.0.1:8899/us.html”显示“我们”；
// 3.5、访问其他url显示“页面找不到”；

// 1引爆
const http = require('http');
const fs = require('fs');
const path = require('path');

// 2使用http模块创建服务器
const server = http.createServer();
// 监听请求
server.on('request', (req, res) => {
    res.writeHead(200, { "Content-Type": "text;charset=UTF-8" });
    if (req.url === '/' || req.url === "/home.html") {
    fs.readFile(path.join(__dirname, '/home.html'), 'utf-8', (error, data) => {
        if (error) {
            console.log(error);
        }
        res.end(data);
    })
} else if (req.url === "/study.html") {
    fs.readFile(path.join(__dirname, '/study.html'), 'utf-8', (error, data) => {
        if (error) {
            console.log(error);
        }
        res.end(data);
    })
} else if (req.url === "/us.html") {
    fs.readFile(path.join(__dirname, '/us.html'), 'utf-8', (error, data) => {
        if (error) {
            console.log(error);
        }
        res.end(data);
    })
}
})

// 指定端口
server.listen(8899, () => {
    console.log("http://127.0.0.1:8899")
})