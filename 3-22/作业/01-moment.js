// 1、使用node.js的moment模块格式输出如下时间格式，要求输出
// 1.1、当前的时间，如（2019-03-20 00:25:01）
// 1.2、当前时间增加1小时
// 1.3、当前时间增加1个月
// 1.4、当前时间的前10天时间
// 1.5、当前时间的前1年时间


//1 引包
const moment = require('moment');
// 用包
moment.locale("zh-CN");
// 以2019-03-20 00:25:01格式输出
console.log(moment().format('YYYY-MM-DD HH:MM:SS'));//当前的时间
console.log(moment().add(1, "hour").format('YYYY-MM-DD HH:MM:SS'));//当前时间增加1小时
console.log(moment().add(1, "mounth").format('YYYY-MM-DD HH:MM:SS'));//当前时间增加1个月
console.log(moment().add(10, "day").format('YYYY-MM-DD HH:MM:SS'));//当前时间的前10天时间
console.log(moment().add(1, "year").format('YYYY-MM-DD HH:MM:SS'));//当前时间的前1年时间