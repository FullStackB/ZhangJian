// 引入包
const express = require('express');
// 通过express创建一个服务器
const app = express();
// 设置静态资源的目录
app.use('/public', express.static("public"));
// 根据不同的url标识符处理不同的请求
app.get('/', (req, res) => {
    res.send(`<!DOCTYPE html>
    <html lang="en">
    
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Document</title>
    </head>
    
    <body>
      <a href="./get.html">get提交方式</a>
      <a href="./post.html">post提交方式</a>
    </body>
    
    </html>`)
})
// 给服务器指定端口启动服务
app.listen(80, () => {
    console.log("http://127.0.0.1");
})