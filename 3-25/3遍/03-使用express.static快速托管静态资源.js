// 引入包
const express = require('express');

// 通过express创建一个服务器
const app = express();

// 设置静态资源的目录就ok了
// app.use(express.static('public'));
app.use('/public', express.static("public"));

// 根据不同的url标识符 处理不同的请求
app.get('/', (req, res) => {
    res.send(`<html>
    <head>
    <body>
    <h1>我就是首页</h1>
    </body>
    </head>
    </html>`);
})

// 给服务器指定端口启动服务
app.listen(80, () => {
    console.log("come on: http://127.0.0.1");
})