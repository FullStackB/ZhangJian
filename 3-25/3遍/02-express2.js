// 引入包
const express = require('express')
// 通过express创建一个服务器
const app = express();
// 根据不同的url标识符处理不同的请求
app.length('/', (req, res) => {
    res.send('首页');
})
// 给服务器指定端口
app.listen(80, () => {
    console.log("来吧 http://127.0.0.1");
})