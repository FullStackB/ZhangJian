// 引入包
const express = require('express');
const bodyParser = require('body-parser');
// 创建服务器
const app = express();
// 设置静态资源目录
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static("public"));

// 监听路径
app.get('/', (req, res) => {
    res.send("首页")
})

app.post('/login', (req, res) => {
    console.log(req.body.username, req.body.password);

    // if (req.query.usename === "me" && req.query.password === "123456") {
    //     res.send("欢迎回家：" + req.query.username);
    // }
    res.send(req.query.username + ',你的密码是' + req.query.password);
})

//  指定端口  启动服务
app.listen(80, () => {
    console.log("http://127.0.0.1");
})