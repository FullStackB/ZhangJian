// 引入包
const express = require('express');
const bodyParser = require('body-parser');
// 创建服务器
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
// 设置静态资源目录
app.use(express.static("public"));
// 监听路径返回响应
app.get('/', (req, res) => {
    res.end("首页");
})
//  使用POST提交表单
app.post('/login', (req, res) => {
    console.log(req.body.username, req.body.password);
    res.send(req.body.username + "<br/>" + req.body.password);
})
// 指定端口启动服务
app.listen(80, () => {
    console.log("服务器运行中...")
})