// 引入包
const http = require('http');
// 根据包创建服务器
const server = http.createServer();
// 监听客户端的request请求事件并处理响应
server.on('request', (req, res) => {
    res.writeHead(200, { "Content-Type": "text/html;charset=UTF-8" });
    if (req.url === '/') {
        res.end("首页")
    }
})
server.listen(80, () => {
    console.log('服务器运行中...');
})